<?php snippet('global/header') ?>
<?php snippet('global/menu-post') ?>

<?php 
    if ($page->numberOfEntries()->isNotEmpty()) {
        $itemsPerView = $page->numberOfEntries()->int();
    } else {
        $itemsPerView = 10;
    }

    // fetch basic articles
    $articles = $page->children()
        ->visible()
        ->flip()
        ->paginate($itemsPerView);

    // add the tag filter
    if($tag = param('tag')) {
        $articles = $articles->filterBy('tags', $tag, ',');
    }

    // apply pagination
    $pagination = $articles->pagination();
?>

<main class="blog row align-center" itemscope itemtype="http://schema.org/Blog">
    <div class="container">
        <div class="row align-center">
            <div class="col-xs-12 col-sm-9">
                <header class="blog__header">
                    <div class="blog__headertext">
                        <div class="content">
                            <p class="dateline" itemprop="about">
                                <?php echo $page->subline()->kirbytextRaw(); ?>
                            </p>
                            <?php echo $page->description()->kirbytext(); ?>
                        </div>
                    </div>
                </header>
            </div>
        </div>
    </div>

    <section class="blog__posts" itemtype="http://schema.org/blogPosts">
        <?php foreach($articles as $article): ?>

            <?php if($article->template() == 'post'): ?>

            <?php if ($bgimage = $article->image($article->coverimage())): ?>
            <div class="blog__posts--with-image" style="background: linear-gradient( rgba(0,0,0,0.7), rgba(0,0,0,0.7) ), url(<?php echo $bgimage->url() ?>) center center/ cover no-repeat; ">
            <?php endif; ?>
                <div class="container">
                    <article class="row align-center">
                        <div class="col-xs-12 col-sm-9">
                            <header class="text blog__posts-header">
                                <time class="kicker blog__time" datetime="<?php echo $article->date('c') ?>" pubdate="pubdate" itemprop="datePublished">
                                    <?php echo $article->date('d.m.Y') ?>
                                </time>
                                <div class="content">
                                    <a class="is-underlined" href="<?php echo $article->url() ?>">
                                        <h3 class="dateline" itemprop="headline">
                                            <?php echo $article->title()->html() ?>
                                        </h3>
                                    </a>

                                    <div class="content__bigtext">
                                        <?php echo $article->short()->kirbytext(); ?>
                                    </div>
                                </div>
                            </header>
                        </div>
                    </article>
                </div>
            <?php if ($bgimage = $article->image($article->coverimage())): ?>
            </div>
            <?php endif; ?>

            <?php elseif($article->template() == 'post.link'): ?>
            <div class="container">
                <article class="row align-center">
                    <div class="col-xs-12 col-sm-9">
                        <header class="text blog__posts-header">
                            <time class="kicker blog__time" datetime="<?php echo $article->date('c') ?>" pubdate="pubdate" itemprop="datePublished">
                                <?php echo $article->date('d.m.Y') ?>
                            </time>
                            <p class="kicker kicker--linkpost" itemprop="headline">
                                <a href="<?php echo $article->link() ?>">
                                    <?php echo $article->title()->html() ?>
                                </a>
                            </p>
                            <?php 
                                $url = $article->link();
                                $urlDomain = parse_url($url);
                            ?>
                            <span class="directlink">
                                <a href="<?php echo $article->link(); ?>">
                                    <?php echo $urlDomain['host']; ?>
                                </a> 
                                /
                                <a href="<?php echo $article->url(); ?>">
                                    Direktlink
                                </a>
                            </span>
                            <div class="content">
                                <div class="content__smalltext">
                                    <?php echo kirbytext($article->short()); ?>
                                </div>
                            </div>
                        </header>
                    </div>
                </article>
            </div>


            <?php endif ?>
        <?php endforeach ?>


        <?php if($pagination->hasPages()): ?>
            <nav class="pagination">
                <ul>
            <?php if($pagination->hasNextPage()): ?>
                <li class="next">
                    <a href="<?php echo $pagination->nextPageURL()?>" title="previous page">&lsaquo;</a>
                </li>
            <?php endif ?>

            <?php foreach($pagination->range($itemsPerView) as $r): ?>
                <li>
                    <a<?php if($pagination->page() == $r) echo ' class="active"' ?> href="<?php echo $pagination->pageURL($r) ?>">
                        <?php echo $r ?>
                    </a>
                </li>
            <?php endforeach ?>
            <?php if($pagination->hasPrevPage()): ?>
                <li class="prev">
                    <a href="<?php echo $pagination->prevPageURL() ?>" title="next page">&rsaquo;</a>
                </li>
            <?php endif ?>
                </ul>
            </nav>
        <?php endif ?>

    </section>
</main>


<?php snippet('global/footer') ?>
