<?php snippet('global/header') ?>
<?php snippet('global/menu-post') ?>
    <main class="blogpost blogpost--single" role="main" itemscope itemtype="http://schema.org/blogposting"> 
        <div class="container">
            <article class="row align-center">
                <header class="col-xs-12 col-sm-8 col-lg-7">
                    <time class="kicker" datetime="<?php echo $page->date('c') ?>" pubdate="pubdate" itemprop="datepublished">
                        <?php echo $page->date('d.m.y') ?>
                    </time>
                    <h2 class="dateline" itemprop="headline"><?php echo $page->title()->html() ?></h2>
                </header>

                <div class="col-xs-12 col-sm-8 col-lg-7">
                    <div class="post" itemprop="articlebody">
                        <?php echo $page->short()->kirbytext()->footnotes() ?>
                        <?php 
                        $url = $page->link();
                        $urlDomain = parse_url($url);
                        ?>
                        <p>
                            <span class="directlink"><a href="<?php echo $page->link(); ?>">Artikel auf <?php echo $urlDomain['host']; ?> lesen</a></span>
                        </p>

                        <ul class="tags" itemprop="keywords">   
                            <?php foreach($page->tags()->split(',') as $tag): ?>   
                                <li>
                                    <a href="<?php echo url($page->parent()->url() . '/' . url::paramsToString(['tag' => $tag])) ?>">
                                        <?php echo html($tag) ?>
                                    </a>
                                </li>
                            <?php endforeach ?> 
                        </ul>


                        <?php snippet('global/author') ?>
                    </div>
                </div>
            </article>
        </div>
    </main>

<?php snippet('global/footer') ?>
