<?php snippet('global/header') ?>
<?php snippet('global/menu-post') ?>


    <main class="blogpost blogpost--single" role="main" itemscope itemtype="http://schema.org/blogposting"> 
        <?php if ($bgimage = $page->image($page->coverimage())): ?>
            <div class="blogpost__header has--bgimage
                <?php if($page->coverheightlarge()->bool()): ?>
                    has--bgimage-large
                <?php endif ?>
                pane" 
                style="background: linear-gradient( rgba(0,0,0,0.7), rgba(0,0,0,0.7) ), url(<?php echo $bgimage->url() ?>) center center/ cover no-repeat; ">

                <div class="container">
                    <div class="row align-center">
                        <header class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                            <div style="position: absolute; width: 100%; bottom: 3rem; max-width: 500px;">
                                <time class="kicker" datetime="<?php echo $page->date('c') ?>" pubdate="pubdate" itemprop="datepublished">
                                    <?php echo $page->date('d.m.y') ?>
                                </time>
                                <h1 class="dateline" itemprop="headline"><?php echo $page->title()->html() ?></h1>
                            </div>
                        </header>
                    </div>
                </div>

                <?php if ($page->covercopyright() != '') : ?>
                    <small class="blogpost__header-copyright">
                        Bildnachweis: 
                        <a href="<?php echo $page->covercopyright() ?>">
                            <?php echo $page->covercopyright() ?>
                        </a>
                    </small>
                <?php endif; ?>
            </div>

        <?php else: ?>
            <div class="blogpost__header pane" >
                <div class="container">
                    <div class="row align-center">
                        <header class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                            <time class="kicker" datetime="<?php echo $page->date('c') ?>" pubdate="pubdate" itemprop="datepublished">
                                <?php echo $page->date('d.m.y') ?>
                            </time>
                            <h2 class="dateline" itemprop="headline"><?php echo $page->title()->html() ?></h2>
                        </header>
                    </div>
                </div>
            </div>
        <?php endif; ?>
            <div class="container">
                <div class="row align-center">
                    <div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
                        <article class="post" itemprop="articlebody"> 
                            <?php echo $page->text()->kirbytext()->footnotes() ?>

                            <ul class="tags" itemprop="keywords">   
                                <?php foreach($page->tags()->split(',') as $tag): ?>   
                                    <li>
                                        <a href="<?php echo url($page->parent()->url() . '/' . url::paramsToString(['tag' => $tag])) ?>">
                                            <?php echo html($tag) ?>
                                        </a>
                                    </li>
                                <?php endforeach ?> 
                            </ul>

                            <?php snippet('global/author') ?>
                        </article>
                    </div>

                </div>
            </div>
        </section>
    </main>

<?php snippet('global/footer') ?>
