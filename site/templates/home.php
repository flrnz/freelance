<?php snippet('global/header') ?>

    <main role="content" itemscope itemtype="https://schema.org/WebPage">

        <?php 
            foreach($pages->visible() as $section) {
                snippet($section->intendedTemplate(), array('data' => $section));
            }
        ?>
    </main>

<?php snippet('global/footer') ?>
