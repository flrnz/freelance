<?php snippet('global/header') ?>
<?php snippet('global/menu-dark') ?>
    <main class="blogpost blogpost--single" role="main"> 
        <?php if ($bgImage = $page->image($page->coverimage())): ?>
            <div class="blogpost__header has--bgimage pane g g--center g--vcenter" style="background: linear-gradient( rgba(0,0,0,0.7), rgba(0,0,0,0.7) ), url(<?php echo $bgImage->url() ?>) center center/ cover no-repeat; ">
                <header class="gi--full">
                    <h1><?php echo $page->title()->html() ?></h1>
                    <hr>
                    <time class="posts__date" datetime="<?php echo $page->date('c') ?>" pubdate="pubdate">
                        <?php echo $page->date('d.m.Y') ?>
                    </time>
                </header>
            </div>
        <?php else: ?>
            <div class="blogpost__header g g--center g--vcenter" style="">
                <header class="gi--full">
                    <h1><?php echo $page->title()->html() ?></h1>
                    <hr>
                    <time class="posts__date" datetime="<?php echo $page->date('c') ?>" pubdate="pubdate">
                        <?php echo $page->date('d.m.Y') ?>
                    </time>
                </header>
            </div>
        <?php endif; ?>
            <div class="g wrap g--center g--wrap">
                <article class="post gi--full gi--sm-3of4"> 
                    <?php echo $page->text()->kirbytext() ?>
                    <section class="gallery gallery--slick">
                        <?php foreach($page->images() as $image): ?>
                        <figure>
                            <img src="<?php echo $image->resize(null, 500)->url(); ?>" alt="">
                        </figure>
                        <?php endforeach ?>
                    </section>
                    <footer> 
                        <ul class="tags">   
                            <?php foreach($page->tags()->split(',') as $tag): ?>   
                            <li><?php echo $tag ?></li>   
                            <?php endforeach ?> 
                        </ul>

                        <?php if($page->pagination()->hasPages()): ?>
                            <nav class="pagination g">
                            <?php if($page->pagination()->hasNextPage()): ?>
                                <a class="next gi--full gi--sm-1of2" href="<?php echo $page->pagination()->nextPageURL() ?>">&lsaquo; newer posts</a>
                            <?php endif ?>
                            <?php if($page->pagination()->hasPrevPage()): ?>
                                <a class="prev gi--full gi--sm-1of2" href="<?php echo $page->pagination()->prevPageURL() ?>">older posts &rsaquo;</a>
                            <?php endif ?>
                            </nav>
                        <?php endif ?>
                    </footer>
                </article>
            </div>
        </section>
    </main>

<?php snippet('global/footer') ?>
