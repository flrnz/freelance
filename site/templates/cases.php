<?php snippet('global/header') ?>
<?php snippet('global/menu-dark') ?>

<main class="blog">
    <section class="blog__header pane">
        <div class="g g--center g--wrap">
            <header class="gi--full">
                <h2> 
                    <?php echo $page->subline()->html() ?>
                </h2>
 
                <div  class="blog__description"> 
                    <?php echo kirbytext($page->description()) ?>
                </div>
                <p> 
                    Abonnieren: 
                    <a class="btn--plain" href="http://twitter.com/track02">
                        <svg class="icon--xsmall icon--twitter" viewBox="0 0 100 100">
                            <use xlink:href="#twitter"></use>
                        </svg>
                        track02
                    </a>
                    <a class="btn--plain" href="/feed">
                        <svg class="icon--xsmall icon--rss" viewBox="0 0 100 100">
                            <use xlink:href="#rss"></use>
                        </svg>
                        RSS
                    </a>
                </p>
            </header>
        </div>
    </section>
    <section>
        <?php $articles = $page->children()->visible()->flip()->paginate(5) ?>
        <?php foreach($articles as $article): ?>
            <?php if ($bgImage = $article->image($article->coverimage())): ?>
                <div class="blogpost__header has--bgimage pane g g--center g--vcenter" 
                    style="
                        background: linear-gradient( rgba(0,0,0,0.7), rgba(0,0,0,0.7) ), 
                                    url(<?php echo $bgImage->url() ?>) center center/ cover no-repeat; 
                        ">
                    <header class="gi--full">
                        <a href="<?php echo $article->url() ?>">
                            <h1><?php echo $article->title()->html() ?></h1>
                        </a>
                        <hr>
                        <time class="posts__date" datetime="<?php echo $article->date('c') ?>" pubdate="pubdate">
                            <?php echo $article->date('d.m.Y') ?>
                        </time>
                    </header>
                </div>
            <?php else: ?>
                <div class="blogpost__header pane pane--prim0 g g--center g--vcenter ">
                    <header class="gi--full">
                        <a href="<?php echo $article->url() ?>">
                            <h1><?php echo $article->title()->html() ?></h1>
                        </a>
                        <hr>
                        <time class="blogpost__date" datetime="<?php echo $article->date('c') ?>" pubdate="pubdate">
                            <?php echo $article->date('d.m.Y') ?>
                        </time>
                    </header>
                </div>
            <?php endif; ?>
        <?php endforeach ?>
        <?php if($articles->pagination()->hasPages()): ?>
            <nav class="pagination g">
            <?php if($articles->pagination()->hasNextPage()): ?>
                <a class="next gi--full gi--sm-1of2" href="<?php echo $articles->pagination()->nextPageURL() ?>">&lsaquo; previous</a>
            <?php endif ?>
            <?php if($articles->pagination()->hasPrevPage()): ?>
                <a class="prev gi--full gi--sm-1of2" href="<?php echo $articles->pagination()->prevPageURL() ?>">next &rsaquo;</a>
            <?php endif ?>
            </nav>
        <?php endif ?>
    </section>
</main>


<?php snippet('global/footer') ?>
