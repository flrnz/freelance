<?php

/*

---------------------------------------
License Setup
---------------------------------------

Please add your license key, which you've received
via email after purchasing Kirby on http://getkirby.com/buy

It is not permitted to run a public website without a
valid license key. Please read the End User License Agreement
for more information: http://getkirby.com/license

*/

c::set('license', 'e6bc2a7eaf268881029ac1d9304a29d5');

/*

---------------------------------------
Kirby Configuration
---------------------------------------

By default you don't have to configure anything to
make Kirby work. For more fine-grained configuration
of the system, please check out http://getkirby.com/docs/advanced/options

*/
c::set('debug', true);
c::set('cache', false);
c::set('cache.ignore', array(
    'blog/feed'
));
c::set('cache.autoupdate',true);
c::set('whoops', false);
c::set('footnotes.global', true);
/* c::set('panel.install', true); */

/* c::set('ssl',true); */


