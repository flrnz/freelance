<?php 
    return function($site, $pages, $page) {
        $form = uniform(
            'contact-form',
            array(
                'required' => array(
                    'name'  => '',
                    '_from' => 'email'
                ),
                'actions' => array(
                    array(
                        '_action' => 'email',
                        'to'      => 'me@example.com',
                        'sender'  => 'info@my-domain.tld',
                        'subject' => 'New Message from johnevanofski.net'
                    )
                )
            )
        );
        return compact('form');
    }
?>
