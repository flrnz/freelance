<section id="<?php echo $data->sectionID(); ?>" class="pane pane--prim0">
    <div class="container">
        <article class="row align-middle <?php if($data->blockorder()->bool() ): ?> g--reversed <?php endif; ?> pane--<?php echo $data->minHeight(); ?>">
            <div class="col-xs-12 col-sm-4
                <?php if($data->svgImage()->isNotEmpty()) : ?>
                col-lg-6
                <?php endif; ?>
                hide-small
            ">
            <?php if($data->svgImage()->isNotEmpty()) : ?>
                <div class="pane__svg">
                    <?php 
                        $sImage = $data->svgImage();
                        echo $data->file($sImage)->read(); 
                    ?>
                </div>
            <?php endif; ?>
            </div>
            <div class="col-xs-12 col-sm-8
                <?php if($data->svgImage()->isNotEmpty()) : ?>
                col-lg-6
                <?php endif; ?>
            ">
                <h2 class="kicker">
                    <?php echo $data->title(); ?>
                </h1>
                <div class="content">
                    <p class="dateline">
                        <?php echo $data->dateline()->kirbytextRaw(); ?>
                    </p>
                    <?php echo kirbytext($data->text()); ?>
                </div>
            </div>
        </article>
    </div>
</section>
