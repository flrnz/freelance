<section id="<?php echo $data->sectionID(); ?>" class="pane pane--prim0 pane--padding">
    <div class="container">
        <article class="row align-middle pane--<?php echo $data->minHeight(); ?>">
            <div class="col-xs-12">

                <div class="text">
                    <div class="row align-center align-middle">
                        <div class="content">
                            <h2 class="kicker gi--full">
                                <?php echo $data->title(); ?>
                            </h2>
                            <p class="dateline gi--full gi--sm-1of2">
                                <?php echo $data->dateline()->kirbytextRaw(); ?>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12">

                <div class="row">
                        <?php foreach( $data->brands()->toStructure() as $brand ): ?>
                    <div class="col-xs-6 col-sm-4 col-md-3" style="text-align: center; margin-bottom: 7rem;">
                            <img style="max-width: 100%; margin: 0; padding: 1rem; max-height: 7rem;" src="<?php echo $data->image($brand->brandimage())->url(); ?>" title="<?php echo $brand->brandname(); ?>">
                    </div>
                        <?php endforeach ?>

                </div>
            </div>
        </article>
    </div>
</section>
