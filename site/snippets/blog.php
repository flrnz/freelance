<section class="pane pane--prim0 pane--blog pane--auto">
    <div class="wrap g"> 
        <h1 class="pane__sectionheader gi--full gi--sm-full">Neueste Artikel</h1>
    </div>
    <div class="wrap g g--center g--vcenter">
        <?php $blog = page('blog') ?>
        <div class="gi--full gi--sm-3of4"> 
        <?php foreach( $blog->children()->visible()->flip()->limit(3) as $article) : ?>
            <article class="post--small">
                <section class="gi--full gi--sm-3of4">
                    <a class="post__readmore" href="<?php echo $article->url() ?>">
                        <header class="post__header text--gsap text--gsap-small">
                            <h1 class="beta">
                                <?php echo $article->title()->html() ?>
                            </h1>
                            <hr>
                            <p class="pane__text pane__text--1col"> 
                                <?php echo $article->short()->html() ?>
                            </p>
                        </header>
                    </a>
                </section>
            </article>
        <?php endforeach ?>
        </div>
        <div class="gi--full gi--sm-1of4">
            <div class="pane__svg gi--full gi--sm-1of4">
                <?php snippet('illustration/notepad') ?>
            </div>
        </div>
    </div>
</section>
