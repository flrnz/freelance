                    <svg version="1.1" id="coffee" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 500 370.8" style="enable-background:new 0 0 500 370.8;" xml:space="preserve">
                        <style type="text/css">
                            #coffee .coffee0{fill:#FFFFFF;}
                            #coffee .coffee1{fill:#603813;}
                            #coffee .coffee2{fill:#E2E2E2;}
                            #coffee .coffee3{fill:none;stroke:#000000;stroke-width:10;stroke-miterlimit:10;}
                        </style>
                        <g id="liquid">
                            <circle class="coffee0" cx="182.3" cy="181.6" r="173"/>
                            <ellipse transform="matrix(0.9733 -0.2294 0.2294 0.9733 -37.5926 48.4381)" class="coffee1" cx="189.6" cy="185.9" rx="147.7" ry="150"/>
                        </g>
                        <path id="gripInner" class="coffee2" d="M345.2,131.6L472,101.7c8.2-1.9,16.3,3.1,18.3,11.3l0,0c1.9,8.2-3.1,16.3-11.3,18.3l-126.8,29.9"/>
                        <circle id="mainOuter" class="coffee3" cx="182.3" cy="181.6" r="173"/>
                        <circle id="mainInner" class="coffee3" cx="182.3" cy="181.6" r="148"/>
                        <path id="gripOuter" class="coffee3" d="M347.7,125.1l118.7-28c8.2-1.9,16.3,3.1,18.3,11.3l0,0c1.9,8.2-3.1,16.3-11.3,18.3l-118.7,28"/>
                    </svg>
