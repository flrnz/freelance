<svg version="1.1" id="meflrnz" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     viewBox="0 0 1882.1 2090.6" style="enable-background:new 0 0 1882.1 2090.6;" preserveAspectRatio="xMidYMax meet" >
    <style type="text/css">
        #meflrnz .st0{fill:#FFE9CC;}
        #meflrnz .st1{fill:#444444;}
        #meflrnz .st2{fill:#232323;}
        #meflrnz .st3{fill:#3488B7;}
        #meflrnz .st4{fill:#3498DB;}
        #meflrnz .st5{fill:none;stroke:#297199;stroke-width:8;stroke-miterlimit:10;}
        #meflrnz .st6{fill:#6D6350;}
        #meflrnz .st7{fill:#FFFFFF;stroke:#000000;stroke-width:16;stroke-miterlimit:10;}
    </style>
    <g id="body">
        <g id="leg_r"> 
            <path class="st1" d="M1041.4,1978.7L1041.4,1978.7c-40.9,0-74.4-33.5-74.4-74.4v-488.1
                c0-40.9,33.5-74.4,74.4-74.4h0c40.9,0,74.4,33.5,74.4,74.4v488.1C1115.7,1945.3,1082.3,1978.7,1041.4,1978.7z"/>
            <path id="shoe_r" class="st2" d="M1118.2,1959.4c0,30.4-33.7,19.3-75,19.3c-41.2,0-74.4,11.1-74.4-19.3s33.4-55,74.7-55
                C1084.7,1904.4,1118.2,1929,1118.2,1959.4z"/>
        </g>
        <g id="leg_l"> 
            <path class="st1" d="M876,1978.7L876,1978.7c-40.9,0-74.4-33.5-74.4-74.4v-488.1c0-40.9,33.5-74.4,74.4-74.4h0
            c40.9,0,74.4,33.5,74.4,74.4v488.1C950.4,1945.3,916.9,1978.7,876,1978.7z"/>
            <path id="shoe_l" class="st2" d="M952.2,1959.4c0,30.4-33.7,19.3-75,19.3s-74.4,11.1-74.4-19.3s33.4-55,74.7-55
            S952.2,1929,952.2,1959.4z"/>
        </g>
        <g id="arm_l">
            <path id="arm_x5F_l" class="st3" d="M746.5,1380.3L746.5,1380.3c-27.4,0-49.9-22.5-49.9-49.9V900.1c0-27.4,22.5-49.9,49.9-49.9h0
                c27.4,0,49.9,22.5,49.9,49.9v430.3C796.4,1357.9,773.9,1380.3,746.5,1380.3z"/>
            <ellipse class="st0" cx="748.5" cy="1405" rx="47.4" ry="52.1"/>
        </g>
        <g id="arm_r">
            <path class="st3" d="M1163.3,1366.9L1163.3,1366.9c-27.4,0-49.9-22.5-49.9-49.9V895.1c0-27.4,22.5-49.9,49.9-49.9h0
                c27.4,0,49.9,22.5,49.9,49.9V1317C1213.2,1344.5,1190.7,1366.9,1163.3,1366.9z"/>
            <ellipse class="st0" cx="1166.3" cy="1397.8" rx="47.4" ry="52.1"/>
        </g>
        <g id="body_1">
            <path id="bodyshirt" class="st4" d="M1073.9,1432.7H825.5c-53.5,0-97.2-43.7-97.2-97.2V891.8c0-53.5,43.7-97.2,97.2-97.2h248.4
                c53.5,0,97.2,43.7,97.2,97.2v443.7C1171.1,1389,1127.4,1432.7,1073.9,1432.7z"/>
            <line id="bodyyip" class="st5" x1="963.6" y1="868.4" x2="963.6" y2="1397.4"/>
            <line id="schnur-l" class="st5" x1="1022" y1="868.4" x2="1022" y2="970.3"/>
            <line id="schnur-r" class="st5" x1="904.7" y1="870" x2="904.7" y2="971.8"/>
        </g>
        <g id="head"> 
            <g id="earsContainer">
                <g id="ears_1_">
                    <path class="st0" d="M746.2,534.8h-47.8c-14.9,0-27-12.2-27-27v-71.7c0-14.9,12.2-27,27-27h47.8c14.9,0,27,12.2,27,27v71.7
                        C773.2,522.7,761,534.8,746.2,534.8z"/>
                    <path class="st0" d="M1201,530.1h-47.8c-14.9,0-27-12.2-27-27v-71.7c0-14.9,12.2-27,27-27h47.8c14.9,0,27,12.2,27,27V503
                        C1228.1,517.9,1215.9,530.1,1201,530.1z"/>
                </g>
            </g>
            <g id="face">
                <ellipse class="st0" cx="949.7" cy="802" rx="71.8" ry="48.3"/>
                <path id="head_1_" class="st0" d="M962.9,793h-26.4c-122.2,0-222.1-99.9-222.1-222.1V329.6c0-122.2,99.9-222.1,222.1-222.1h26.4
                    c122.2,0,222.1,99.9,222.1,222.1v241.3C1185,693.1,1085,793,962.9,793z"/>
            </g>
            <g id="mustache">
                <path id="mustacheInner" class="st6" d="M1083.2,672.4c-0.8-36.2-30.1-65.6-65.8-65.6H880.8c-36.2,0-65.8,30.2-65.8,67.1v0
                    c0,0,0,0.1,0,0.1c-62.5-34.6-104.2-134.8-104.2-54.7v0c0,95.6,68.1,173.8,151.4,173.8h175.1c83.2,0,151.4-78.2,151.4-173.8v0
                    C1188.6,540.3,1145.2,636.4,1083.2,672.4z"/>
            </g>
            <path id="hair" class="st6" d="M1157.9,210.3C1157.9,210.3,1157.9,210.3,1157.9,210.3c-39-64.7-111.7-108.4-194.5-108.4h-38.3
                c-103.3,0-190.9,68.1-217,160c1.5-1.9,3-3.7,4.4-5.5c-16.6,63.2-4.2,147.8-4.2,147.8l57.3-202.7c63.9-51,101.9-19.7,175.1-19.7
                c65.1,0,115-34.1,184.7,15.3l58.6,207.1C1184.2,404.3,1204.6,264.9,1157.9,210.3z"/>
            <g id="glasses_2_">
                <path class="st7" d="M895.2,475.1H766.5c-17.7,0-32.2-14.5-32.2-32.2v-62.8c0-17.7,14.5-32.2,32.2-32.2h128.8
                    c17.7,0,32.2,14.5,32.2,32.2v62.8C927.4,460.6,912.9,475.1,895.2,475.1z"/>
                <path class="st7" d="M1133,475.1h-128.8c-17.7,0-32.2-14.5-32.2-32.2v-62.8c0-17.7,14.5-32.2,32.2-32.2H1133
                    c17.7,0,32.2,14.5,32.2,32.2v62.8C1165.2,460.6,1150.7,475.1,1133,475.1z"/>
            </g>
        </g>
    </g>
</svg>

