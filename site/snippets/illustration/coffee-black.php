<!-- Generator: Adobe Illustrator 19.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Kaffee" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="109 -82 976 695" style="enable-background:new 109 -82 976 695;" xml:space="preserve">
<style type="text/css">
	#Kaffee .st0{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:6;stroke-miterlimit:10;}
	#Kaffee .st1{fill:#FEFEFE;stroke:#FFFFFF;stroke-width:6;stroke-miterlimit:10;}
	#Kaffee .st2{fill:#2D2D2D;}
	#Kaffee .st3{fill:#474747;}
</style>
<g>
	<g id="Mug">
		<circle class="st0" cx="463.2" cy="294" r="286.7"/>
		<path class="st1" d="M737.4,200.5L934,154.2c13.5-3.2,27.1,5.2,30.3,18.7l0,0c3.2,13.5-5.2,27.1-18.7,30.3l-196.7,46.4"/>
	</g>
	<g id="cInner">
		<circle class="st2" cx="463.2" cy="294.1" r="245.2"/>
		<ellipse id="cHightlight" class="st3" cx="479.6" cy="274.2" rx="195.8" ry="177.4"/>
	</g>
</g>
</svg>
