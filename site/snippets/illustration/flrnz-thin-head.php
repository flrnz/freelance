<!-- Generator: Adobe Illustrator 19.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="mehead" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="1240.9 -745.6 805.6 898.2" style="enable-background:new 1240.9 -745.6 805.6 898.2;" xml:space="preserve">
<style type="text/css">
	#mehead .st0{fill:#FFE9CC;}
	#mehead .st1{display:none;fill:#FFE9CC;}
	#mehead .st2{fill:#6D6350;}
	#mehead .st3{fill:#FFFFFF;stroke:#000000;stroke-width:16;stroke-miterlimit:10;}
</style>
<g id="body">
	<g id="head">
		<g id="earsContainer">
			<g id="ears_1_">
				<path class="st0" d="M1437.2-210.8h-47.8c-14.9,0-27-12.2-27-27v-71.7c0-14.9,12.2-27,27-27h47.8c14.9,0,27,12.2,27,27v71.7
					C1464.2-222.9,1452-210.8,1437.2-210.8z"/>
				<path class="st0" d="M1892-215.5h-47.8c-14.9,0-27-12.2-27-27v-71.7c0-14.9,12.2-27,27-27h47.8c14.9,0,27,12.2,27,27v71.6
					C1919.1-227.7,1906.9-215.5,1892-215.5z"/>
			</g>
		</g>
		<g id="face">
			<ellipse class="st1" cx="1640.7" cy="56.4" rx="71.8" ry="48.3"/>
			<path id="head_1_" class="st0" d="M1653.9,47.4h-26.4c-122.2,0-222.1-99.9-222.1-222.1V-416c0-122.2,99.9-222.1,222.1-222.1h26.4
				c122.2,0,222.1,99.9,222.1,222.1v241.3C1876-52.5,1776,47.4,1653.9,47.4z"/>
		</g>
		<g id="mustache">
			<path id="mustacheInner" class="st2" d="M1774.2-73.2c-0.8-36.2-30.1-65.6-65.8-65.6h-136.6c-36.2,0-65.8,30.2-65.8,67.1l0,0v0.1
				c-62.5-34.6-104.2-134.8-104.2-54.7l0,0c0,95.6,68.1,173.8,151.4,173.8h175.1c83.2,0,151.4-78.2,151.4-173.8l0,0
				C1879.6-205.3,1836.2-109.2,1774.2-73.2z"/>
		</g>
		<path id="hair" class="st2" d="M1848.9-535.3L1848.9-535.3c-39-64.7-111.7-108.4-194.5-108.4h-38.3c-103.3,0-190.9,68.1-217,160
			c1.5-1.9,3-3.7,4.4-5.5c-16.6,63.2-4.2,147.8-4.2,147.8l57.3-202.7c63.9-51,101.9-19.7,175.1-19.7c65.1,0,115-34.1,184.7,15.3
			l58.6,207.1C1875.2-341.3,1895.6-480.7,1848.9-535.3z"/>
		<g id="glasses_2_">
			<path class="st3" d="M1586.2-270.5h-128.7c-17.7,0-32.2-14.5-32.2-32.2v-62.8c0-17.7,14.5-32.2,32.2-32.2h128.8
				c17.7,0,32.2,14.5,32.2,32.2v62.8C1618.4-285,1603.9-270.5,1586.2-270.5z"/>
			<path class="st3" d="M1824-270.5h-128.8c-17.7,0-32.2-14.5-32.2-32.2v-62.8c0-17.7,14.5-32.2,32.2-32.2H1824
				c17.7,0,32.2,14.5,32.2,32.2v62.8C1856.2-285,1841.7-270.5,1824-270.5z"/>
		</g>
	</g>
</g>
</svg>
