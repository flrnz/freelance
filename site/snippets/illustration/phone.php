<svg version="1.1" id="phoneTop" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    viewBox="-276 351.1 448 540" style="enable-background:new -276 351.1 448 540;" xml:space="preserve">
    <style type="text/css">
        #phoneTop .st0{fill:#F4F4F4;}
        #phoneTop .st1{fill:#FFFFFF;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}
        #phoneTop .st2{fill:#424242;}
        #phoneTop .st3{fill:#555555;}
        #phoneTop .st4{fill:none;stroke:#000000;stroke-width:4;stroke-miterlimit:10;}
        #phoneTop .st5{clip-path:url(#SVGID_2_);}
        #phoneTop .st6{fill:#FFFFFF;}
        #phoneTop .st7{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.5342;stroke-miterlimit:10;}
        #phoneTop .st8{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.4964;stroke-miterlimit:10;}
        #phoneTop .st9{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.5155;stroke-miterlimit:10;}
        #phoneTop .st10{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.521;stroke-miterlimit:10;}
        #phoneTop .st11{fill:#FFFFFF;stroke:#3498DB;stroke-width:5;stroke-miterlimit:10;}
        #phoneTop .st12{fill:none;stroke:#3498DB;stroke-width:5;stroke-miterlimit:10;}
        #phoneTop .st13{fill:#3498DB;stroke:#3498DB;stroke-width:1.1321;stroke-miterlimit:10;}
        #phoneTop .st14{fill:none;stroke:#3498DB;stroke-width:3.3964;stroke-miterlimit:10;}
        #phoneTop .st15{fill:#3498DB;stroke:#3498DB;stroke-width:0.7909;stroke-miterlimit:10;}
        #phoneTop .st16{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.4002;stroke-miterlimit:10;}
        #phoneTop .st17{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.3134;stroke-miterlimit:10;}
        #phoneTop .st18{fill:#3498DB;stroke:#FFFFFF;stroke-width:0.3536;stroke-miterlimit:10;}
    </style>
    <g id="phone">
            <rect x="-167.3" y="398.3" transform="matrix(-1 1.799742e-03 -1.799742e-03 -1 -92.3867 1224.084)" class="st0" width="241.1" height="427.5"/>

            <rect x="-174.4" y="391.2" transform="matrix(-1 1.799742e-03 -1.799742e-03 -1 -106.5994 1209.8967)" class="st1" width="241.1" height="427.5"/>
            <path class="st2" d="M82,854.5c0.1,15.2-11,27.5-24.6,27.5l-221.5,0.4c-13.6,0-24.7-12.3-24.7-27.3l-0.8-464.5
            c-0.1-15.2,11-27.5,24.6-27.5l221.5-0.4c13.6,0,24.7,12.3,24.7,27.3L82,854.5z"/>

            <ellipse transform="matrix(1.799742e-03 1 -1 1.799742e-03 796.9974 902.2063)" class="st3" cx="-53.4" cy="850.3" rx="17.7" ry="17.8"/>
        <line class="st4" x1="-83.7" y1="375.8" x2="-24.8" y2="375.8"/>
    </g>
    <g id="wireframe">
        <g>
            <g>
                <defs>
                    <polygon id="SVGID_1_" points="-173,397.4 67.9,395.7 66.4,811.6 -174.4,813.3                "/>
                </defs>
                <clipPath id="SVGID_2_">
                    <use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
                </clipPath>
                <g id="wireframeInner" class="st5">
                    <g>
                        <rect x="-173" y="397.4" class="st6" width="247.2" height="828.1"/>
                        <g id="upper">
                            <polygon class="st7" points="47.8,624 -157.5,624.3 -157.5,613.9 48,613.6                        "/>
                            <polygon class="st8" points="19.9,643.1 -157.6,643.4 -157.6,632.9 19.9,632.7                        "/>
                            <polygon class="st9" points="33.7,661.4 -157.6,661.7 -157.6,651.3 33.7,651                      "/>
                            <polygon class="st10" points="37.8,680.5 -157.7,680.8 -157.6,670.3 37.8,670.1                       "/>
                            <polygon class="st7" points="48.2,697.9 -157.2,698.1 -157.2,687.7 48.3,687.5                        "/>
                            <polygon class="st8" points="20.2,717.1 -157.2,717.3 -157.2,706.9 20.2,706.6                        "/>
                            <polygon class="st11" points="48,592.7 -157.4,593 -157.1,454.7 48.4,454.4                       "/>
                            <line class="st12" x1="-157.1" y1="454.7" x2="48" y2="592.7"/>
                            <line class="st12" x1="-157.4" y1="593" x2="48.4" y2="454.4"/>
                        </g>
                        <g id="HEADER">
                            <polygon class="st7" points="-64.4,429.3 -157.9,429.5 -157.8,409.4 -64.3,409.3                      "/>
                            <g id="burger">
                                <path class="st13" d="M21.1,417.6l19.3-0.1c0,0,3.5-0.3,1.9-4.4c-1.7-4.2-5.4-6.8-10.3-6.8c-4.8,0-9.9,0-11.9,5.7
                                    C18,417.6,21.1,417.6,21.1,417.6z"/>
                                <line class="st14" x1="20.1" y1="422.1" x2="42.3" y2="422"/>
                                <path class="st15" d="M40.5,426.4l-19.3,0.4c0,0-3.5,0.2-1.8,2.2s5.4,3.2,10.3,3.1c4.9-0.1,9.8-0.2,12-2.9
                                    C43.6,426.3,40.5,426.4,40.5,426.4z"/>
                            </g>
                        </g>
                        <g id="paragraph">
                            <polygon class="st9" points="33.5,803.3 -157.7,803.5 -157.7,793.1 33.5,792.8                        "/>
                            <polygon class="st10" points="37.6,822.4 -157.8,822.7 -157.8,812.2 37.6,812                         "/>
                            <polygon class="st7" points="48.1,839.8 -157.3,840.1 -157.2,829.6 48.1,829.3                        "/>
                            <polygon class="st8" points="20,859 -157.4,859.2 -157.3,848.8 20,848.5                      "/>
                            <polygon class="st16" points="-107,783.1 -157.2,783.1 -157.1,759.1 -107,759                         "/>
                            <polygon class="st17" points="-69.3,783 -99.9,783 -99.9,759 -69.2,759                       "/>
                            <polygon class="st18" points="-23.3,782.9 -62.3,783 -62.2,759 -23.1,758.9                       "/>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
