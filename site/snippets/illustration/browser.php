<!-- Generator: Adobe Illustrator 19.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="mockup" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="465.1 -22 916.8 637.6" xml:space="preserve">
<style type="text/css">
	#mockup .st0{fill:#FFFFFF;}
	#mockup .st1{fill:#CFCFCF;}
	#mockup .st2{fill:#E45D55;}
	#mockup .st3{fill:#E9BC33;}
	#mockup .st4{fill:#7CC04F;}
	#mockup .st5{fill:#EBEBEB;}
	#mockup .st6{fill:#C7C7C7;}
	#mockup .st8{display:inline;fill:#7CC04F;fill-opacity:0.4;}
	#mockup .st9{display:inline;opacity:0.3;fill:#7EC052;fill-opacity:0.5;enable-background:new    ;}
	#mockup .st10{display:inline;fill:#7CC04F;}
	#mockup .st11{font-family:'MyriadPro-Bold';}
	#mockup .st12{font-size:18.0474px;}
	#mockup .st13{fill:#6D6D6D;fill-opacity:0.3;}
	#mockup .st14{fill:#60A1EF;}
	#mockup .st15{fill:#91BFED;}
</style>
<g id="Browser">
	<path class="st0" d="M472.2-22h902.7c3.9,0,7.1,3.9,7.1,8.8v597.4c0,4.9-3.2,8.8-7.1,8.8H472.2c-3.9,0-7.1-3.9-7.1-8.8V-13.2
		C465.1-18.1,468.3-22,472.2-22z"/>
	<path class="st1" d="M465.1,76.6v-93c0-3.1,3.2-5.6,7.1-5.6h902.7c3.9,0,7.1,2.5,7.1,5.6v93H465.1z"/>
	<circle class="st2" cx="490.1" cy="4.4" r="8.3"/>
	<circle class="st3" cx="519.2" cy="4.4" r="8.3"/>
	<circle class="st4" cx="548.4" cy="4.4" r="8.3"/>
	<path class="st5" d="M465.1,78V26.6h107.5H591c3.2,0,5.4-1.5,6.2-3.1C601.3,15.4,611.4-4,611.4-4c1.4-2.1,3.3-2.8,5.3-2.8h156.9
		H793c1.9,0,3.9,0.6,5.3,2.8c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0.1c0,0,0,0,0,0c0,0,0,0.1,0,0.1c0,0,0,0,0,0
		c0,0,0,0.1,0.1,0.1c0,0,0,0,0,0.1c0.1,0.1,0.1,0.2,0.2,0.4c0,0,0,0,0,0c0,0.1,0.1,0.2,0.1,0.2c0,0,0,0,0,0c0,0.1,0.1,0.2,0.1,0.2
		c0,0,0,0,0,0.1c0,0.1,0.1,0.2,0.1,0.2c0,0,0,0,0,0.1c0,0.1,0.1,0.2,0.1,0.3c0,0,0,0,0,0.1c0.2,0.3,0.4,0.7,0.6,1.1c0,0,0,0,0,0
		c0.1,0.1,0.1,0.2,0.2,0.4c0,0,0,0,0,0.1c0.1,0.1,0.1,0.2,0.2,0.4c0,0,0,0.1,0.1,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0,0,0,0.1,0.1,0.1
		c0.2,0.5,0.5,1,0.8,1.5c0,0,0,0,0,0.1c0.1,0.2,0.2,0.3,0.2,0.5c0,0,0,0.1,0.1,0.1c0.1,0.1,0.2,0.3,0.2,0.4c0,0,0,0.1,0.1,0.1
		c0.1,0.2,0.2,0.3,0.2,0.5c0,0,0,0.1,0,0.1c0.1,0.2,0.2,0.3,0.3,0.5c0,0,0,0.1,0,0.1c0.2,0.4,0.4,0.8,0.6,1.2c0,0,0,0,0,0
		c0.1,0.2,0.2,0.4,0.3,0.6c0,0,0,0.1,0.1,0.1c0.1,0.2,0.2,0.3,0.3,0.5c0,0.1,0.1,0.1,0.1,0.2c0.1,0.2,0.2,0.3,0.2,0.5
		c0,0,0.1,0.1,0.1,0.1c0.1,0.2,0.2,0.3,0.3,0.5c0,0,0,0.1,0.1,0.1c0.1,0.2,0.2,0.4,0.3,0.6c0,0,0,0,0,0c0.1,0.2,0.2,0.4,0.3,0.7l0,0
		c3.7-7,7.1-13.6,7.1-13.6c1.4-2.1,3.3-2.8,5.3-2.8h156.9H994c1.9,0,3.9,0.6,5.3,2.8c0,0,10.1,19.4,14.2,27.5
		c0.8,1.6,3.1,3.1,6.2,3.1h18.3h343.8V78H465.1z"/>
	<g>
		<path class="st6" d="M797.7-5.7l16.7,33.3h1.4L799.1-5.7H797.7z"/>
		<path class="st1" d="M814.4,27.6v-1.4h213.8v1.4H814.4z"/>
		<path class="st0" d="M595.2,36.3h722c2.1,0,3.8,1.2,3.8,2.8v26.4c0,1.5-1.7,2.8-3.8,2.8h-722c-2.1,0-3.8-1.2-3.8-2.8V39.1
			C591.4,37.6,593.1,36.3,595.2,36.3z"/>
		<g class="st7">
			<path class="st8" d="M616.4,47.4v12.5c0,1.5-1.2,2.8-2.8,2.8h-9.7c-1.5,0-2.8-1.2-2.8-2.8V44.6c0-1.5,1.2-2.8,2.8-2.8h6.9
				L616.4,47.4z"/>
			<path class="st9" d="M611.8,48.8L611.8,48.8h-0.9c-0.8,0-1.4-0.6-1.4-1.4v-1l0,0v-4.5h1.2l5.7,5.7v1.2H611.8z"/>
			<text transform="matrix(1 0 0 1 624.2798 58.5187)" class="st10 st11 st12">SSL</text>
		</g>
	</g>
	<path class="st13" d="M566.5,51.6L566.5,51.6l3.6-3.7c-1.1-1.1-2.7-1.8-4.4-1.8c-3.4,0-6.2,2.8-6.2,6.2c0,3.4,2.8,6.2,6.2,6.2
		c2.2,0,4.1-1.1,5.2-2.8h1.9l1.8,1.9c-1.8,3-5.1,5.1-8.9,5.1c-5.8,0-10.4-4.7-10.4-10.4s4.7-10.4,10.4-10.4c2.9,0,5.4,1.2,7.3,3l3-3
		h0.1v9.7H566.5z"/>
	<path class="st13" d="M496.3,54.4h-11.8l3.5,3.5c0.8,0.8,0.8,2.1,0,2.9c-0.8,0.8-2.1,0.8-2.9,0l-6.9-6.9c0,0,0-0.1-0.1-0.1
		c-0.2-0.2-0.3-0.4-0.4-0.6c0,0,0,0,0,0c-0.2-0.5-0.2-1.2,0-1.7c0.1-0.3,0.3-0.6,0.6-0.8L485,44c0.8-0.8,2.1-0.8,2.9,0
		c0.8,0.8,0.8,2.1,0,2.9l-3.2,3.2h11.6c1.1,0,2.1,0.9,2.1,2.1C498.4,53.4,497.5,54.4,496.3,54.4z"/>
	<path class="st13" d="M537.1,53.2C537.1,53.2,537.1,53.2,537.1,53.2c-0.1,0.2-0.2,0.4-0.4,0.6c0,0,0,0.1-0.1,0.1l-6.9,6.9
		c-0.8,0.8-2.1,0.8-2.9,0c-0.8-0.8-0.8-2.1,0-2.9l3.5-3.5h-11.8c-1.2,0-2.1-0.9-2.1-2.1c0-1.1,0.9-2.1,2.1-2.1h11.6l-3.2-3.2
		c-0.8-0.8-0.8-2.1,0-2.9c0.8-0.8,2.1-0.8,2.9,0l6.7,6.7c0.3,0.2,0.5,0.5,0.6,0.8C537.3,52,537.3,52.7,537.1,53.2z"/>
	<path class="st13" d="M1362.5,54.4h-13.9c-1.1,0-2.1-0.9-2.1-2.1c0-1.1,0.9-2.1,2.1-2.1h13.9c1.2,0,2.1,0.9,2.1,2.1
		C1364.6,53.4,1363.6,54.4,1362.5,54.4z M1362.5,47.4h-13.9c-1.1,0-2.1-0.9-2.1-2.1s0.9-2.1,2.1-2.1h13.9c1.2,0,2.1,0.9,2.1,2.1
		S1363.6,47.4,1362.5,47.4z M1348.6,57.1h13.9c1.2,0,2.1,0.9,2.1,2.1c0,1.1-0.9,2.1-2.1,2.1h-13.9c-1.1,0-2.1-0.9-2.1-2.1
		C1346.5,58.1,1347.4,57.1,1348.6,57.1z"/>
	<path class="st13" d="M1361.2-1.8l-2.8,2.8c-0.8,0.8-2.1,0.8-2.9,0c-0.8-0.8-0.8-2.1,0-2.9l2.8-2.8l-3.4-3.4h9.6v9.7L1361.2-1.8z
		 M1351.5,7.9l3.4,3.4h-9.7V1.6l3.4,3.4l2.8-2.8c0.8-0.8,2.1-0.8,2.9,0c0.8,0.8,0.8,2.1,0,2.9L1351.5,7.9z"/>
</g>
<rect x="465.1" y="78" class="st14" width="916.8" height="537.7"/>
<rect x="465.1" y="78" class="st15" width="210.6" height="537.7"/>
<rect x="498.4" y="146" class="st14" width="131.6" height="22.2"/>
<rect x="498.4" y="187.5" class="st14" width="131.6" height="22.2"/>
<rect x="498.4" y="229.2" class="st14" width="131.6" height="22.2"/>
<rect x="498.4" y="274.4" class="st14" width="131.6" height="22.2"/>
<rect x="729.6" y="146" class="st15" width="469.6" height="29.1"/>
<rect x="729.6" y="196.7" class="st15" width="388.5" height="29.1"/>
<rect x="729.6" y="284.5" class="st15" width="557.4" height="14.5"/>
<rect x="729.6" y="315.1" class="st15" width="528.6" height="14.5"/>
<rect x="729.6" y="346.8" class="st15" width="540.4" height="14.5"/>
<rect x="729.6" y="379.1" class="st15" width="482.8" height="14.5"/>
</svg>
