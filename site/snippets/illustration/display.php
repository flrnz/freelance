<svg version="1.1" id="display" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 740.7 586.9" style="enable-background:new 0 0 740.7 586.9;" xml:space="preserve">
    <style type="text/css">
        #display .st0{fill:none;stroke:#FFFFFF;stroke-width:5;stroke-miterlimit:10;}
        #display .st1{fill:none;stroke:#FEDF0D;stroke-width:5;stroke-miterlimit:10;}
        #display .st2{fill:#C9C9C9;stroke:#C9C9C9;stroke-width:5;stroke-miterlimit:10;}
        #display .st3{fill:#DFDFDF;stroke:#DFDFDF;stroke-width:5;stroke-miterlimit:10;}
        #display .st4{fill:#FFFFFF;stroke:#C9C9C9;stroke-width:5;stroke-miterlimit:10;}
        #display .st5{fill:#2271BA;stroke:#2271BA;stroke-width:5;stroke-miterlimit:10;}
    </style>
    <path class="st0" d="M268.6,78.3"/>
    <path class="st0" d="M83.1,78.3"/>
    <path class="st1" d="M320,94.4"/>
    <rect id="standUpper" x="324.6" y="407" class="st2" width="77.1" height="65.1"/>
    <path id="standBottom" class="st3" d="M446.8,482.4c0,4-2.1,7.2-4.7,7.2h-158c-2.6,0-4.7-3.2-4.7-7.2l0,0c0-4,2.1-7.2,4.7-7.2h158
        C444.7,475.3,446.8,478.5,446.8,482.4L446.8,482.4z"/>
    <path id="screenOuter" class="st3" d="M677.5,389c0,10-7.9,18.1-17.6,18.1H66.3c-9.7,0-17.6-8.1-17.6-18.1V47.2
        c0-10,7.9-18.1,17.6-18.1h593.6c9.7,0,17.6,8.1,17.6,18.1V389z"/>
    <rect id="screenInner" x="65" y="57.6" class="st4" width="596.3" height="311.6"/>
    <g id="Code">
        <line id="line1" class="st5" x1="87.5" y1="87.2" x2="530.1" y2="87.2"/>
        <line id="line2" class="st5" x1="187.1" y1="147.3" x2="465.5" y2="147.3"/>
        <line id="line3" class="st5" x1="139.5" y1="177.4" x2="387.1" y2="177.4"/>
        <line id="line4" class="st5" x1="87.5" y1="207.5" x2="542.6" y2="207.5"/>
        <line id="line5" class="st5" x1="139.5" y1="237.6" x2="594" y2="237.6"/>
        <line id="line6" class="st5" x1="205.6" y1="267.6" x2="619.7" y2="267.6"/>
        <line id="line7" class="st5" x1="139.5" y1="297.7" x2="163.3" y2="297.7"/>
        <line id="line8" class="st5" x1="134.8" y1="115.2" x2="519.9" y2="115.2"/>
    </g>
</svg>
