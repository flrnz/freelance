<section id="<?php echo $data->sectionID(); ?>" class="pane pane--prim0">
    <div class="container">
        <article class="row align-middle <?php if($data->blockorder()->bool() ): ?> g--reversed <?php endif; ?> pane--<?php echo $data->minHeight(); ?>">

            <div class="col-xs-12 col-sm-4 col-md-6">
                <div class="atom-wrapper">
                    <div class="atom">
                        <div class="atom__nucleus"></div>
                        <div class="atom__electron">
                            <div class="atom__electron-inner"></div>
                        </div>
                        <div class="atom__electron">
                            <div class="atom__electron-inner"></div>
                        </div>
                        <div class="atom__electron">
                            <div class="atom__electron-inner"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-6">

                <div class="text">
                    <h2 class="kicker">
                        <?php echo $data->title(); ?>
                    </h1>
                    <div class="content">
                        <p class="dateline">
                            <?php echo $data->dateline()->kirbytextRaw(); ?>
                        </p>
                        <?php echo kirbytext($data->text()); ?>
                    </div>
                </div>

            </div>
        </article>
    </div>
</section>
