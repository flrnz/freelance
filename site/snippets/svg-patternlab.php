<section id="<?php echo $data->sectionID(); ?>" class="pane pane--prim1 pane--padding pane--<?php echo $data->minHeight(); ?>">
    <div class="container">
        <div class="row align-center">
            <div class="col-xs-12 col-sm-12 svg--full-centered">

                <svg version="1.1" id="ATOMIC" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 985 978.9" style="enable-background:new 0 0 985 978.9;" xml:space="preserve">
                    <style type="text/css">
                        #ATOMIC .st0{fill:#BaBaBa;}
                        #ATOMIC .st1{fill:#2271BA;}
                        #ATOMIC .st2{fill:#1C1C1C;}
                        #ATOMIC .st3{fill:#f1f1f1;}
                    </style>

                    <circle id="backcircle" fill="#f1f1f1" cx="500" cy="501" r="357.5"/>
                    <g id="BLOCK1">
                        <rect id="BG1" x="236.4" y="178.2" class="st0" width="512.1" height="234.8"/>
                        <rect id="Headline" x="264.8" y="240.4" class="st1" width="227.7" height="29.2"/>
                        <g id="Paragraph_1_">
                            <rect x="264.8" y="306.7" class="st2" width="270.3" height="10.8"/>
                            <rect x="264.8" y="328.7" class="st2" width="234" height="10.8"/>
                            <rect x="264.8" y="350.7" class="st2" width="247.4" height="10.8"/>
                        </g>
                        <circle class="st1" cx="650.9" cy="301" r="57.5"/>
                    </g>
                    <g id="BLOCK2">
                        <rect id="BG2" x="236.4" y="412.9" class="st0" width="512.1" height="162.6"/>
                        <g id="ParagraphSM1">
                            <rect id="B2HeadlineSmall1" x="264.8" y="441" class="st1" width="120.8" height="14.6"/>
                            <g id="Paragraph2">
                                <rect x="264.8" y="478.6" class="st2" width="131.2" height="10.8"/>
                                <rect x="264.8" y="500.6" class="st2" width="113.6" height="10.8"/>
                                <rect x="264.8" y="522.7" class="st2" width="120.1" height="10.8"/>
                            </g>
                        </g>
                        <g id="ParagraphSM2">
                            <rect id="B2HeadlineSmall2" x="421.8" y="441.2" class="st1" width="120.8" height="14.6"/>
                            <g id="Paragraph2_1_">
                                <rect x="421.8" y="478.9" class="st2" width="131.2" height="10.8"/>
                                <rect x="421.8" y="500.8" class="st2" width="113.6" height="10.8"/>
                            </g>
                        </g>
                        <g id="ParagraphSM3">
                            <rect id="B2HeadlineSmall3" x="584.3" y="442" class="st1" width="98.4" height="14.6"/>
                            <g id="Paragraph2_2_">
                                <rect x="584.3" y="479.7" class="st2" width="113.6" height="10.8"/>
                                <rect x="584.3" y="501.7" class="st2" width="98.4" height="10.8"/>
                                <rect x="584.3" y="523.7" class="st2" width="104" height="10.8"/>
                            </g>
                        </g>
                    </g>
                    <g id="BLOCK3">
                        <rect id="BG1_1_" x="236.4" y="575.6" class="st1" width="512.1" height="234.8"/>
                        <rect id="HEADLINE3" x="266" y="638.9" class="st0" width="225.3" height="14.2"/>
                        <g id="Paragraph3">
                            <rect x="266" y="676.7" class="st2" width="157" height="10.8"/>
                            <rect x="266" y="698.7" class="st2" width="135.9" height="10.8"/>
                            <rect x="266" y="720.8" class="st2" width="143.7" height="10.8"/>
                        </g>
                    </g>
                </svg>

            </div>
            <div class="col-sm-12 text">
                <div class="row align-center">
                    <div class="content">
                        <p class="dateline col-xs-12 col-sm-6">
                            <?php echo $data->dateline()->kirbytextRaw(); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

