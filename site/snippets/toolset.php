<section id="toolset" class="toolset pane pane--prim0 pane--me pane--auto">
    <div class="wrap ">
        <section class="s-about g g--center g--wrap">
            <div class="gi--full gi--sm-2of3 text--gsap">
                <h1 class="underlined"><?php echo $data->title() ?></h1>
                <hr>
                <div class="pane__text">
                    <?php echo kirbytext($data->text()) ?>
                </div>
            </div>
            <div class="gi--full gi--sm-1of3"> </div>
            <ul class="toolset__icons gi--full">
                <?php $partners = yaml($data->icons()) ?>
                <?php foreach( $partners as $partner ): ?>
                    <li data-skill="<?php echo $partner['skill'] ?>">
                        <?php if ($iconlink = $partner['iconlink']): ?>
                        <a href="<?php echo $partner['iconlink'] ?>" title="<?php echo $partner['iconlinkdescription'] ?>">
                            <?php if($image = $data->image( $partner['iconimage'] )): ?>
                                <img src="<?php echo $image->url() ?>" title=<?php echo $partner['iconlinkdescription'] ?>">
                            <?php endif ?>
                        </a>
                        <?php else: ?>
                            <?php if($image = $data->image( $partner['iconimage'] )): ?>
                                <img src="<?php echo $image->url() ?>" title=<?php echo $partner['iconlinkdescription'] ?>">
                            <?php endif ?>
                        <?php endif; ?>
                    </li>
                <?php endforeach ?>
            </ul>  
            <div class="gi--full"> 
                <?php snippet('global/responsebuttons', array('data' => $data)) ?>
            </div>
        </section>
    </div>
</section>
