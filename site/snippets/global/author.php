<aside class="post__author g">
    <figure class="gi--1of3 gi--sm-1of4 post__authorimage">
        <img 
            src="/content/<?php echo $site->authorimage() ?>" 
            alt="<?php echo $site->author() ?>" />
    </figure>
    <div class="gi--2of3 gi--sm-3of4 post__authordescription">
        <?php echo kirbytext($site->description()) ?>
    </div>
</aside>
