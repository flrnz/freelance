<aside class="head" role="banner">
    <div class="row">
        <div class="col-sm-12">
            <!-- <?php echo $site->image($site->authorimage())->resize(50); ?> -->       
            <nav class="navigation" role="navigation">
                <ul class="var3">
                <?php foreach($pages->visible() as $section) : ?>
                    <?php if ( $section->sectionID()->isNotEmpty() ) : ?>
                        <li>
                            <a class="js-scroll" href="#<?php echo $section->sectionID() ?>">
                                <?php echo $section->sectionName() ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
                    <li>
                        <a href="<?php echo $site->find('blog')->url(); ?>">
                            <?php echo $site->find('blog')->title(); ?>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</aside>
