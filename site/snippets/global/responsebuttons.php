<?php if(!$data->buttonNext()->empty() || !$data->buttonText()->empty() ): ?>
    <ul class="btn-group responsebuttons hide-on-small"> 
        <?php if(!$data->buttonNext()->empty() ): ?>
        <li>
            <a class="btn scroll" href="<?php echo $data->nextButtonId() ?>">
                <svg class="icon--small icon--light" viewBox="0 0 100 100">
                    <use xlink:href="#arrow-down"></use>
                </svg>
                <?php echo $data->buttonNext() ?>
            </a>
        </li>
        <?php endif; ?>
        <?php if(!$data->buttonText()->empty() ): ?>
        <li>
            <a class="btn scroll" href="<?php echo $data->buttonId() ?>">
                <svg class="icon--small icon--light" viewBox="0 0 100 100">
                    <use xlink:href="#arrow-right"></use>
                </svg>
                <?php echo $data->buttonText() ?>
            </a>
        </li>
        <?php endif; ?>
    </ul>
<?php endif; ?>
