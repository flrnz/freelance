    <header class="head head--blog" role="banner">
        <div class="wrap g g--center g--wrap"> 
            <h1 class="gi--full gi--sm-3of4">
                <span class="head__logo"> 
                    <?php snippet('illustration/flrnz-thin-head') ?>
                </span>
                <span class="is-vishidden" aria-hidden="true"> 
                    <?php echo $site->title() ?>
                </span>
            </h1>
            <?php if($page->template() =='post' ): ?>
            <a class="head__back" href="<?php echo $page->parent()->url() ?>">
                &larr; Zurück
            </a>
            <?php endif; ?>
        </div>
    </header>
