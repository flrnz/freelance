<header class="head head--blog" role="banner">
    <div class="container">
        <div class="row align-center">
            <div class="col-xs-6 col-sm-3">
                <h1 class="head__title">
                    <a href="<?php echo $page->site->url() ?>">
                        <span class="head__logo" aria-hidden="true"> 
                            <?php echo $site->image($site->authorimage())->resize(50); ?> 
                        </span>
                    </a>
                    <?php if ($page->template() == 'post')  : ?>
                    <a href="<?php echo $page->parent()->url() ?>">
                        <span class="head__directory" aria-hidden="true">
                            / <?php echo $page->parent->dirname(); ?>
                        </span>
                    </a>
                    <?php elseif ($page->template() == 'posts') : ?>
                        <span class="head__directory" aria-hidden="true">
                            / <?php echo $page->dirname(); ?>
                        </span>
                    <?php else : ?>
                    <a href="<?php echo $page->parent()->url() ?>">
                        <span class="head__directory" aria-hidden="true">
                            / <?php echo $page->parent->dirname(); ?>
                        </span>
                    </a>
                    <?php endif ?>
                    <span class="is-vishidden" aria-hidden="false"> 
                        <?php echo $site->title() ?>
                    </span>
                </h1>
            </div>

            <div class="col-xs-6 col-sm-6">
                <nav class="head__icons navigation--icons gi gi--1of3 gi--sm-1of3" role="navigation">
                    <ul>
                        <li>
                            <a class="btn--plain" itemprop="sameAs" href="http://twitter.com/<?php echo $site->twitter(); ?>">
                                <svg class="icon--small icon--twitter" viewBox="0 0 100 100">
                                    <use xlink:href="#twitter"></use>
                                </svg>
                                <span class="is-vishidden" aria-hidden="false">
                                    <?php echo $site->twitter(); ?>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a class="btn--plain" href="/blog/feed">
                                <svg class="icon--small icon--rss" viewBox="0 0 100 100">
                                    <use xlink:href="#rss"></use>
                                </svg>
                                <span class="is-vishidden" aria-hidden="false">
                                    RSS
                                </span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
