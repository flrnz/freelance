    <footer class="footer pane pane--prim4" role="contentinfo">
        <div class="container"> 
            <div class="row align-center">
                <p><small>
                    <?php echo $site->copyright()->kirbytext() ?>
                </small></p>
            </div>
        </div>
    </footer>
    <?php echo js('assets/js-bin/bundle.js') ?>
    <?php echo js('assets/js/prism.js') ?>
    <?php echo js('assets/oembed/assets/js/oembed.js') ?>
    <?php snippet('global/analytics') ?>
</body>
</html>
