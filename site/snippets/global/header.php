<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <title><?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Neuton:400,700|Unica+One">
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="description" content="<?php echo $site->description()->html() ?>">
    <meta name="keywords" content="<?php echo $site->keywords()->html() ?>">
    <meta name="robots" content="index, follow">

    <meta name="google-site-verification" content="pn80JBRubq-NmHjRC3pL4D26IN_o9Esmd3MBRAfEgrE" />

    <meta name="author" content="Florenz Heldermann">
    <meta name="DC.title" content="<?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?>">
    <meta name="DC.subject" content="<?php echo $site->description()->html() ?>">

    <meta property="og:title" content="<?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?>">
    <meta property="og:type" content="blog">
    <meta property="og:url" content="<?php echo $page->url() ?>">
    <meta property="og:image" content="/assets/favicon/ms-icon-144x144.png">
    <meta property="og:site_name" content="Florenz Heldermann">
    <meta property="og:description" content="<?php echo $site->description()->html() ?>">

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@track02" />

    <?php if ($page->template() == 'post')  : ?>
        <meta name="twitter:title" content="<?php echo  $page->title()->html() ?>" />
        <meta name="twitter:description" content="<?php echo $page->short()->html() ?>" />
        <?php if ($page->coverimage() != '') :?>
            <meta name="twitter:image" content="<?php echo $page->image($page->coverimage())->url() ?>" />
        <?php else : ?>
            <meta name="twitter:image" content="<?php echo $site->authorimage()->url() ?>" />
        <?php endif ?>

    <?php elseif ($page->template() == 'posts') : ?>
        <meta name="twitter:title" content="<?php echo $site->title()->html() ?> | <?php echo $page->title()->html() ?>" />
        <meta name="twitter:description" content="<?php echo $page->seodescription()->html() ?>" />
        <meta name="twitter:image" content="https://pbs.twimg.com/profile_images/2551715132/temp1346144393strip20120828-15620-1t778v6" />

    <?php else : ?>
        <meta name="twitter:title" content="<?php echo $site->title()->html() ?>" />
        <meta name="twitter:description" content="<?php echo $site->description()->html() ?>" />
        <meta name="twitter:image" content="https://pbs.twimg.com/profile_images/2551715132/temp1346144393strip20120828-15620-1t778v6" />
    <?php endif ?>


    <link rel="alternate" type="application/rss+xml" href="<?php echo url('blog/feed') ?>" title="<?php echo html($pages->find('blog/feed')->title()) ?>" />

    <?php echo css('assets/css/main.css') ?>
    <?php echo css('assets/css/prism.css') ?>
    <?php echo css('assets/oembed/assets/css/oembed.css') ?>
    <?php echo css('assets/css/svganimation.css') ?>
    <?php echo js('assets/js/modernizr.js') ?>

</head>
<body>
    <?php snippet('global/svg-icons') ?>

    <div class="modal modal--top modal--wide modal--update">
        <div class="modal__inner">
            <?php echo $site->ancientbrowser()->kirbytext() ?>
        </div>
    </div>
