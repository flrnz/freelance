<section id="intro" class="intro pane pane--prim1 pane--full">
    <div class="container">
        <div class="row align-middle">
            <header class="col-xs-12 col-sm-8 col-md-7" itemscope itemtype="https://schema.org/Person">
                <?php echo snippet('global/menu'); ?>

                <h1 itemprop="name" class="intro__title">
                    <?php echo $site->title(); ?>
                </h1>
                <p class="pane__text dateline" itemprop="description"> 
                    <?php echo $data->text() ?>
                </p>
                <ul class="quickcontact"> 
                    <li> 
                        <a href="mailto:<?php echo $site->email() ?>" itemscope itemtype="https://schema.org/Email">
                            <svg class="icon--small icon--light" viewBox="0 0 100 100">
                                <use xlink:href="#mail2"></use>
                            </svg>
                            <?php echo kirbytext($site->email()) ?>
                        </a>
                    </li>
                    <li>
                        <a href="tel:<?php echo $site->phone() ?>" itemprop="telephone">
                            <svg class="icon--small icon--light" viewBox="0 0 100 100">
                                <use xlink:href="#phone-iphone"></use>
                            </svg>
                            <?php echo kirbytext($site->phone()) ?>
                        </a>
                    </li>
                    <li> 
                        <a itemprop="sameAs" href="http://twitter.com/<?php echo $site->twitter() ?>">
                            <svg class="icon--small icon--light" viewBox="0 0 100 100">
                                <use xlink:href="#twitter"></use>
                            </svg>
                            <?php echo kirbytext($site->twitter()) ?>

                        </a>
                    </li>
                </ul>
            </header>
            <div class="col-xs-12 col-sm-4 col-md-5 intro__image">
                <img itemprop="image" src="<?php echo $data->image($data->avatar())->url() ?>" alt="">
            </div>
        </div>
    </div>
</section>
