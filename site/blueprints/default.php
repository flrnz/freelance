<?php if(!defined('KIRBY')) exit ?>

title: Default
pages: true
files: true
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
  blockorder:
    label: Blockreihenfolge
    type: checkbox
    text: Vertauscht Bild und Text Reihenfolge.
  nextButtonId:
    label: next button link id
    type: text
    width: 1/2
  buttonNext:
    label: Text for next anchor
    type: text
    width: 1/2
    default: Next
  buttonId:
    label: button link id
    type: text
    width: 1/2
    default: #contact
  buttonText:
    label: buttontext
    type: text
    width: 1/2
    default: Schreiben Sie mich an
