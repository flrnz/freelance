<?php if(!defined('KIRBY')) exit ?>

title: Intro
pages: false
fields:
  title:
    label: Title
    type:  text
  introtitle:
    label: Introtitel
    type:  text
  text:
    label: Einleitungstext
    type:  textarea
    size:  large
  avatar:
    label: Image of me
    type: image
