<?php if(!defined('kirby')) exit ?>

title: Blogpost
pages:false
files:
  sortable:true
  fields:
    caption:
      label: caption
      type: textarea
fields:
  title:
    label: title
    type:  text
  short:
    label: Kurzbeschreibung
    type: textarea
    required: true
  date:
    label: datum
    type: date
    default: today
    width: 1/2
  author:
    label: author
    type: user
    width: 1/2
  coverimage:
    label: coverimage
    type: image
    width: 1/2
  covercopyright:
    label: cover copyright
    type: text
    width: 1/2
  coverheightlarge:
    label: very large headerimage?
    type: checkbox
    text: make the header image full page height big. the image quality should be good!
  text:
    label: text
    type: markdown
    required: true
  tags:
    label: tags
    type: tags
    lowercase: true
  category:
    label: category
    type: select
    options:
      allgemein: allgemein
      frontend: frontend
      tutorial: tutorial

