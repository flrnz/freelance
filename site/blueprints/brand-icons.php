<?php if(!defined('KIRBY')) exit ?>

title: Brands and Customers
pages: false
files:
  sortable:true
fields:
  title:
    label: Title
    type:  text
  dateline:
    label: Einleitung
    type:  textarea
  minHeight:
    label: Min-Height of Section
    type: select
    options: 
      auto: Automatisch
      full: Voller Bildschirm (100vh)
      half: Halber Bildschirm (50vh)
      third: Drittel Bildschirm (33vh)
  brands:
    label: Icons
    type: structure
    entry: >
      {{brandimage}}<br>
      {{brandname}}
    fields:
      brandimage:
        label: Icon
        type: select
        options: images
      brandname:
        label: Icon link description
        type: text
