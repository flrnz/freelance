<?php if(!defined('KIRBY')) exit ?>

title: Referenzen
pages:false
files:
  sortable:true
fields:
  title:
    label: Title
    type:  text
  short:
    label: Kurzbeschreibung
    type: text
    required: true
  text:
    label: Text
    type: textarea
    required: true
  blockorder:
    label: Blockreihenfolge
    type: checkbox
    text: Vertauscht Bild und Text Reihenfolge.
  nextButtonId:
    label: next button link id
    type: text
    width: 1/2
  buttonNext:
    label: Text for next anchor
    type: text
    width: 1/2
    default: Next
  buttonId:
    label: button link id
    type: text
    width: 1/2
    default: #contact
  buttonText:
    label: buttontext
    type: text
    width: 1/2
    default: Schreiben Sie mich an
