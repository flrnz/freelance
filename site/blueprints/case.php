<?php if(!defined('KIRBY')) exit ?>

title: Case
pages:false
files:
  sortable:true
fields:
  title:
    label: Title
    type:  text
  short:
    label: Kurzbeschreibung
    type: text
    required: true
  date:
    label: Datum
    type: date
    default: today
    width: 1/2
  year:
    label: Jahr
    type: text
    width: 1/2
  coverimage:
    label: Coverimage
    type: select
    options: images
  tags:
    label: Tags
    type: tags
    lowercase: true
  text:
    label: Text
    type: textarea
    required: true

