<?php if(!defined('KIRBY')) exit ?>

title: Cases
files: false
fields:
  title:
    label: Titel
    type:  text
  subline:
    label: Subline
    type:  text
  Description:
    label: Blog Description
    type:  textarea
