<?php if(!defined('KIRBY')) exit ?>

title: Posts
pages:
  template: 
    - post
    - post.link
  num: date
  sort: flip
files: false
fields:
  title:
    label: Titel
    type:  text
  subline:
    label: Subline
    type:  text
  description:
    label: Blog Description (with Links)
    type:  textarea
  seodescription:
    label: Seo Description
    type:  textarea
  numberOfEntries:
    label: Anzahl der Blogeinträge pro Seite
    type:  text
