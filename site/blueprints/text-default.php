<?php if(!defined('KIRBY')) exit ?>

title: Text (Standard)
pages: false
files: true
fields:
  title:
    label: Title
    type:  text
  dateline:
    label: Einleitung
    type:  textarea
  text:
    label: Text
    type:  textarea
  svgImage:
    label: SVG File
    type:  image
    info: Der Inhalt wird als Inline Element eingefügt
  sectionID:
    label: ID für Main Navigation
    type:  text
    width: 1/2
  sectionName:
    label: Name für Main Navigation
    type:  text
    width: 1/2
  blockorder:
    label: Blockreihenfolge
    type: checkbox
    text: Vertauscht Bild und Text Reihenfolge.
    width: 1/2
  minHeight:
    label: Min-Height of Section
    type: select
    options: 
      auto: Automatisch
      full: Voller Bildschirm (100vh)
      half: Halber Bildschirm (50vh)
      third: Drittel Bildschirm (33vh)
    width: 1/2
