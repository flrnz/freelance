<?php if(!defined('KIRBY')) exit ?>

title: Animation Patternlab
pages: false
files: true
fields:
  title:
    label: Title
    type:  text
  dateline:
    label: Einleitung
    type:  textarea
  sectionID:
    label: ID für Main Navigation
    type:  text
    width: 1/2
  sectionName:
    label: Name für Main Navigation
    type:  text
    width: 1/2
  isQuote:
    label: Text ein Zitat?
    type: checkbox
    text: Setzt um das Text Element ein Blockquote
    width: 1/2
  minHeight:
    label: Min-Height of Section
    type: select
    options: 
      auto: Automatisch
      full: Voller Bildschirm (100vh)
      half: Halber Bildschirm (50vh)
      third: Drittel Bildschirm (33vh)
    width: 1/2
