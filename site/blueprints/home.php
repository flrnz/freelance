<?php if(!defined('KIRBY')) exit ?>

title: Home
pages: false
fields:
  title:
    label: Title
    type:  text
  introtitle:
    label: Introtitel
    type:  text
  text:
    label: Einleitungstext
    type:  textarea
    size:  large
  nextButtonId:
    label: next button link id
    type: text
    width: 1/2
  buttonNext:
    label: Text for next anchor
    type: text
    width: 1/2
    default: Next
  buttonId:
    label: button link id
    type: text
    width: 1/2
    default: #contact
  buttonText:
    label: buttontext
    type: text
    width: 1/2
    default: Schreiben Sie mich an
