<?php if(!defined('kirby')) exit ?>

title: Linkpost
pages: false
files: false
icon: link
fields:
  title:
    label: title
    type:  text
  link:
    label: URL
    type: text
    required: true
  short:
    label: Kurzbeschreibung
    type: textarea
    required: true
  date:
    label: datum
    type: date
    default: today
    required: true
    width: 1/2
  author:
    label: author
    type: user
    width: 1/2
  tags:
    label: tags
    type: tags
    lowercase: true
    width: 1/2
  category:
    label: category
    type: select
    width: 1/2
    options:
      allgemein: Allgemein
      frontend: Frontend
      tutorial: Tutorial
      linkpost: Linkpost

