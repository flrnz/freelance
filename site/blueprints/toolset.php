<?php if(!defined('KIRBY')) exit ?>

title: Toolset
pages:false
files:
  sortable:true
fields:
  title:
    label: Title
    type:  text
  short:
    label: Kurzbeschreibung
    type: text
    required: true
  text:
    label: Text
    type: textarea
    required: true
  nextButtonId:
    label: next button link id
    type: text
    width: 1/2
  buttonNext:
    label: Text for next anchor
    type: text
    width: 1/2
    default: Next
  buttonId:
    label: button link id
    type: text
    width: 1/2
    default: #contact
  buttonText:
    label: buttontext
    type: text
    width: 1/2
    default: Schreiben Sie mich an
  icons:
    label: Icons
    type: structure
    entry: >
      {{iconimage}} {{skill}}<br>
      {{iconlink}} <br>
      {{iconlinkdescription}}
    fields:
      iconimage:
        label: Icon
        type: select
        options: images
      iconlink:
        label: Icon Link 
        type: text
      iconlinkdescription:
        label: Icon link description
        type: text
      skill:
        label: Skill
        type: text
