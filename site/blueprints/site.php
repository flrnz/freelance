<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: true
fields:
  title:
    label: Title
    type:  text
  author:
    label: Author
    type:  text
    width: 1/2
  authorimage:
    label: Author Image
    type:  image
    width: 1/2
  description:
    label: Description
    type:  textarea
  email:
    label: Emailadresse
    type: text
    width: 1/2
  phone:
    label: Telefonnummer
    type: text
    width: 1/2
  twitter:
    label: Twitter
    type: text
  keywords:
    label: Keywords
    type:  tags
  activateGoogleOptimize
    label: Aktiviere Google Optimize
    type: checkbox
    text: Aktiviere Google Optimize A/B Testing. Kann Auswirkung auf Ladeverhalten haben.
  copyright:
    label: Copyright
    type:  textarea
  ancientbrowser:
    label: Browserupdate
    type:  textarea
