Title: Wer ist Florenz?

----

Text: 

Ausserdem schreibe ich nicht gerne in der dritten Person über mich.

Während meines Studiums der “Digitalen Medien” in Bremen habe ich mich nicht nur mit Webtechnologien beschäftigt, sondern auch Prinzipien der Gestaltung im Bewegtbild angeeignet. 

Seit 2011 habe ich bei den größten Agenturen Bremens als Frontendentwickler angestellt. Hier konnte ich nicht nur viel Erfahrung in größeren Projekte gewinnen, sondern auch meine Fähigkeiten weiter ausbauen. Anfang 2016 habe ich mein Angestellten Leben nur noch Teilzeit ausgeführt und habe mich als Freelance Frontend Entwickler selbstständig gemacht. Eine Entscheidung die ich nicht bereute und bereits 7 Monate später meine Festanstellung beendete um mich Vollzeit auf meine Freelance Karriere zu konzentrieren. 

Ich bin begeisterter Frontend Konferenzbesucher - für mich eine Möglichkeit mein eigenes Wissen mit interessanten Vorträgen zu erweitern. Schon in der Agentur hmmh habe ich oft Vorträge und Weiterbildungen zu Frontend Technologien durchgeführt. Darum habe ich 2014 die Möglichkeit wahrgenommen mich als Co-Organisator des Bremer (link: http://webmontag-bremen.de text: Webmontag) zu betätigen. Unser Auftrag ist es alle drei Monate einen interessanten Abend mit Vorträgen aus dem Webbereich im Raum Bremen zu unterhalten und Diskussionen anzuregen.

----

Blockorder: 1

----

Dateline: Florenz ist ein Bremer Frontendentwickler mit mehr als 10 Jahren Erfahrung. Wenn er nicht gerade Webseiten baut, über (link: /blog text: Webentwicklung schreibt) oder  Sprecher für den (link: http://www.webmontag-bremen.de text: Bremer Webmontag) sucht, nimmt er auch gerne (link: http://halbwissen.co text: Podcasts) auf oder entspannt mit (link: http://twitter.com/mllebambi text: Kathrin) bei einem gutem Film. Gelegentlich darf er auch mal Videospiele spielen.

----

Svgimage: 

----

Sectionid: 

----

Sectionname: 

----

Minheight: full