Title: Wofür ich stehe

----

Text: Ich liebe das Web und bin glücklich mit meiner Arbeit einen Teil dazu beitragen zu können. Bereits in der Frühzeit des Internets bewaffnet mit einer *index.html* und einer *style.css* hat sich der Aspekt der Frontend Entwicklung stark weiterentwickelt. Mit (link: /blog text: starkem Interesse) für aktuelle Entwicklungen in diesem Bereich ist es mir wichtig immer am Zahn der Zeit zu sein. Neue Themen und Ansätze ziehen mich an und eine schnelle, performante und auch nutzerfreundliche Webseite ist heute mit das wichtigste Element eines erfolgreichen Projekts.

----

Blockorder: 1

----

Tags: Responsive Design,HTML5,Atomic Design,CSS3,SCSS,Javascript,jQuery,Gulp,Grunt,Git,Jenkins,Illustrator,Photoshop,Podcasting,Greensock GSAP,Wordpress,Kirby,Typo3

----

Buttonid: #contact

----

Buttontext: Projektanfrage

----

Nextbuttonid: #frontend

----

Buttonnext: Weiter

----

Sectionid: about-me

----

Dateline: Mit mehrjähriger Erfahrung in kleinen und großen Projekten biete ich nicht nur semantisch-korrektes HTML und performantes CSS, sondern auch viel angewandtes Wissen in den Bereichen Interaction Design und Usability, als auch ein Auge für Gestaltung.

----

Svgimage: 

----

Sectionname: Über mich

----

Minheight: full