Title: Interessiert?

----

Dateline: Du hast ein spannendes Projekt und würdest gerne  mit mir zusammen arbeiten? Schreib mich einfach unter (email: hi@flore.nz text: hi@flore.nz) an.

----

Text: Ansonsten findest du mich auch auf (link: https://www.xing.com/profile/Florenz_Heldermann text: Xing), (link: https://de.linkedin.com/in/florenzheldermann text: LinkedIn) oder (link: http://github.com/heroheman text: GitHub). Mein Privatleben offenbare ich auch (link: http://twitter.com/track02 text: Twitter) und (link: https://instagram.com/flrnz/ text: Instagram).

----

Svgimage: flrnz-circle.svg

----

Sectionid: contact

----

Sectionname: Kontakt

----

Blockorder: 1

----

Minheight: full