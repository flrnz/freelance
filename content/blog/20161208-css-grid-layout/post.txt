Title: CSS Grid Layout

----

Short: Frames, Tabellen, Floats, Flexbox... und jetzt: Grid Layouts!

----

Date: 2016-12-08

----

Author: florenz

----

Coverimage: radpack_2.jpg

----

Covercopyright: http://www.natewren.com/rad-pack-80s-themed-hd-wallpapers/

----

Coverheightlarge: 0

----

Text: 

Gestern ist das CSS Grid Layout in der (link: https://www.mozilla.org/de/firefox/developer/ text: Firefox Developer Edition) gelandet. Da war es zwar schon vorher, aber hinter einer Flag versteckt. Für mich Grund genug mir dieses ~~garnicht mal so~~ neue Feature einmal genauer anzuschauen. In Videoform. 

(oembed: https://www.youtube.com/watch?v=WgQ-md36ne0 lazyload: true)
Sollte euch das Video gefallen, (link: https://www.youtube.com/channel/UCOThsgNv5DkHuxTp2TxXgJg text: abonniert meinen Kanal ) und / oder lasst einen Like da. 

### Grundlagen: Layout
Das Grid-Layout kann man sich wortwörtlich als Gitter vorstellen, das Elternelement  bekommt - genau wie bei Flexbox -  eine `display` Property am umfassenden Element. Danach wird mit `grid-template-columns` und `grid-template-rows` jeweils die die Anzahl der Spalten und Zeilen gesetzt. 
(image: css-grid-layout-figure-1.jpg caption: Abb 1 - Das CSS Grid-Layout)
Hier ist etwas Umdenken erforderlich. Bisherige Ansätze waren nur auf Spalten reduziert. Die Dimensionen einer HTML Komponente direkt mit Koordinaten zu bestimmen ist ein recht neues Konzept. Sobald man aber dran gewöhnt ist und verstanden hat wie es funktioniert ist es recht intuitiv und durchdacht. 

Die nachfolgenden Beispiel und Ergebnisse sind (link: http://codepen.io/heroheman/pen/RoJZbr text: auf Codepen zu finden). Und wenn ihr schon dort seid - (link: http://codepen.io/heroheman/ text: folgt mir doch gleich dort). 

#### Einfaches 4-Spalten Layout
Ein Beispiel:
````html
<div class="grid grid-col-3">
    <div class="item item-1">col 1</div>
    <div class="item item-2">col 2</div>
    <div class="item item-3">col 3</div>
    <div class="item item-4">col 3</div>
</div>
````
````css
.grid {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-template-rows: 10vh;
}
````

* `grid-template-columns` zeigt die Anzahl der Spalten. Hier werden vier gleich große Einheiten gesetzt. 
* Analog zeigt `grid-template-rows` die Anzahl der Reihen. In diesem Fall nur eine Reihe. 
* Die Einheit `fr` steht für *fraction* - also für den Teil der übrigen  Gesamtbreite oder Länge. Es kann aber auch *px*, *rem* oder jede ander Einheit verwendet werden. 


#### Ein 2x2 Layout
Ein einfaches 2x2 Layout wäre auch schnell umzusetzen. Hierfür muss einfach nur der entspechende Wert in `grid-template-row` ergänzt werden. 
````css
grid-template-columns: 1fr 1fr;
grid-template-rows: 10vh 10vh;
````
Das Ergebnis gibt es auf der (link: http://codepen.io/heroheman/pen text: Demoseite auf Codepen.) 



#### Responsive
Das klappt natürlich auch *responsive*. Hierfür einfach wie üblich in Media Queries verschachteln und schon könnte man mit wenigen Anpassungen komplexere Webseiten erstellen.
````css
.grid {
	grid-template-columns: 1fr 1fr;
   	grid-template-rows: 10vh 10vh;
   
   	@media screen and (min-width: 768px) {
      	grid-template-columns: 1fr 1fr 1fr 1fr;
      	grid-template-rows: 10vh;
   	}
}
````
Das Ergebnis gibt es - zusammen mit weiteren Demos von mir - auf der (link: http://codepen.io/heroheman/pen text: Demoseite auf Codepen.) Den Browser hierfür einmal vergrößern und verkleinern. 


### Grundlagen: Positionierung
Wie man ein Grid Layout erstellt haben wir jetzt hinter uns, aber wie positioniert man jetzt Elemente in diesem Grid? Es gibt zwei Ansätze - einmal mit direkten Koordinaten und einmal mit benamten Bereichen. 

#### Positionierung mit Grid-Columns und Grid-Rows
Die Kindelemente können mit `grid-column-start` und `grid-column-end` bzw. `grid-row-start` und `grid-row-end` gesetzt werden. 

````html
<div class="grid">
  <div class="item item-1">col 1</div>
  <div class="item item-2">col 2</div>
  <div class="item item-3">col 3</div>
  <div class="item item-4">col 4</div>
</div>
```` 
````css
.grid {
   display: grid;
   grid-template-columns: 1fr 1fr 1fr 1fr;
   grid-template-rows: 10vh 10vh 10vh;
   
   .item-1 {
      grid-column-start: 1;
      grid-column-end: 5;
      grid-row: 1;
   }
   .item-2 {
      grid-column-start: 1;
      grid-column-end: 3;
      grid-row: 2;
   }
   .item-3 {
      grid-column-start: 4;
      grid-column-end: 5;
      grid-row: 2;
   }
   .item-4 {
      grid-column-start: 1;
      grid-column-end: 5;
      grid-row: 3;
   }
}

````
Das Ergebnis gibt es auf der (link: http://codepen.io/heroheman/pen text: Demoseite auf Codepen.)

(image: bildschirmfoto-2016-12-09-um-13.21.42.png caption: Abb. 2 - CSS Grid-Layout)

Die Start und Endpunkte beziehen sich auf die Linien und nicht auf die Spalten oder Reihen. Somit kann es auch einen *Endpunkt 5* geben, obwohl augenscheinlich nur 4 Spalten vorhanden sind. Siehe dazu das Schaubild *Abb.1*. 

Man kann auch die Shorthand nutzen, diese vereinigt Start- und Endpunkt:
````css
.item-1 {
	// grid-column-start: 1;
    // grid-column-end: 3;
    grid-column: 1 / 3;
    // grid-row-start: 2;
    // grid-row-end: 3;
    grid-row: 2/3;
}
````

#### Positionierung mit Grid-Areas
Anstatt nun jeweils x und y Koordinaten zu setzen gibt es noch den Weg mit benamten Bereichen. Hierfür wird auf `grid-*start` und `grid-*-end` verzichtet und einfach nur `grid-area` verwendet. Der Wert ist hier ein frei wählbarer Name. Hier ein CSS Snippet: 
````css
.grid {
   grid-template-columns: 1fr 1fr 1fr 1fr;
   grid-template-rows: 10vh 10vh 10vh;
   grid-template-areas: "home home home home"
                        "feed feed . about"
                        "imprint imprint imprint imprint";
   
   .item-1 {
      grid-area: home;
   }
   .item-2 {
      grid-area: feed;
   }
   .item-3 {
      grid-area: about;
   }
   .item-4 {
      grid-area: imprint;
   }
}
````
Das Markup ist identisch zum vorherigen Beispiel. 

Hier ist die Property `grid-template-areas` am Grid-Selektor wichtig. Es wird eine Anzahl von Strings übergeben, wobei jeder String eine Reihe darstellt. Schreibt man jetzt viermal *Header*, füllt das Header Element vier Spalten aus. In der zweiten Zeile sieht man, dass *feed* zwei Spalten einnimmt und *about* nur eine Spalte. Der `.` zeigt eine leere Spalte an. 

Das Ergebnis sollte ungefähr so aussehen:
(image: bildschirmfoto-2016-12-09-um-14.24.51.png caption: Abb. 3 - Ein CSS Grid Layout mit benamten Bereichen)

(link: http://codepen.io/heroheman/pen text: Hier sieht man es nochmal in Action). Auch ein paar Beispiele wie ein kleines Layout so dargestellt werden kann. 

### Browser Support
Zum jetzigen Zeitpunkt* (Dezember 2016)* ist der (link: http://caniuse.com/#feat=css-grid text: Browsersupport noch recht mau). Lediglich der Internet Explorer hat das CSS Grid Layout integriert. Allerdings mit einer veralteten Syntax. Wem das bekannt vor kommt - dasselbe ist damals auch mit Flexbox und Internet Explorer 10 und 11 passiert. 

Chrome, Firefox und Opera unterstützen zwar inzwischen das *CSS Grid Layout*, aber diese Funktion ist derzeit nur mit dem Setzen einer Flag aktivierbar. 

Dennoch bin ich zuversichtlich, dass in spätestens einem Jahr die ersten Webseiten auf das *CSS Grid* setzen und dass es in spätestens zwei Jahren genauso einsetzbar ist wie heute Flexbox. 

### Gibt es noch mehr? 
Ja! Viel mehr. Ich habe erstmal nur die Grundlagen des CSS Grid beschrieben. Alle Möglichkeiten hat Chris Coyier in seinem (link: https://css-tricks.com/snippets/css/complete-guide-grid/ text: beeindruckenden Artikel / Cheatsheet) zum Thema zusammen getragen. 
Zum Beispiel, dass die Grid Linien nicht unbedingt *1, 2, 3, etc* heissen müssen sondern auch echte Benamungen haben können. Oder wie man die Ausrichtung der einzelnen Elemente zielsicher steuern kann.

### TL;DR
* Grid Layout mit `display: grid`
* Reihen mit `grid-template-rows`
* Spalten mit `grid-template-columns`
* Positionen lassen sich mit `grid-row` / `grid-columns` setzen
* Alternativ kann man benamte Bereiche mit `grid-area` und `grid-areas` setzen
* CSS-Grid-Layouts haben bislang sehr schlechten Support, aber wird sehr relevant werden

### Achja!
**Artikel wie diese sind sehr viel Arbeit und freue mich über jedes Feedback.** Sollte euch der Artikel gefallen haben, schreibt (email: hi@flore.nz text: per Mail) oder auf (link: http://twitter.com/track02 text: Twitter) und abonniert meinen (link: https://www.youtube.com/channel/UCOThsgNv5DkHuxTp2TxXgJg text: YouTube Channel)! Vorallem Feedback zu dem Video ist sehr willkommen.

----

Tags: CSS,Tutorial,Video

----

Category: frontend