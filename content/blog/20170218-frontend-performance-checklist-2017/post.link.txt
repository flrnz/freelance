Title: Frontend Performance Checklist 2017

----

Link: https://www.smashingmagazine.com/2016/12/front-end-performance-checklist-2017-pdf-pages/

----

Short: 

Das (link: https://www.smashingmagazine.com/ text: Smashing Magazine) hat seine "(link:https://www.smashingmagazine.com/2016/12/front-end-performance-checklist-2017-pdf-pages/ text: Frontend Performance Checklist 2017)" veröffentlicht. Insgesamt 33 Tips, inklusive PDF.   

Kurz zusammengefasst ergeben sich folgende 10 Tipps:
>1. Your goal is a start rendering time under 1 second on cable and under 3 seconds on 3G, and a SpeedIndex value under 1000. Optimize for start rendering time and time-to-interactive.
2. Prepare critical CSS for your main templates, and include it in the <head> of the page. (Your budget is 14 KB).
3. Defer and lazy-load as many scripts as possible, both your own and third-party scripts — especially social media buttons, video players and expensive JavaScript.
4. Add resource hints to speed up delivery with faster dns-lookup, preconnect, prefetch, preload and prerender.
5. Subset web fonts and load them asynchronously (or just switch to system fonts instead).
6. Optimize images, and consider using WebP for critical pages (such as landing pages).
7. Check that HTTP cache headers and security headers are set properly.
8. Enable Brotli or Zopfli compression on the server. (If that’s not possible, don’t forget to enable Gzip compression.)
9. If HTTP/2 is available, enable HPACK compression and start monitoring mixed-content warnings. If you’re running over LTS, also enable OCSP stapling.
10. If possible, cache assets such as fonts, styles, JavaScript and images — actually, as much as possible! — in a service worker cache.

----

Date: 2017-02-18

----

Author: florenz

----

Tags: perfmatters

----

Category: linkpost