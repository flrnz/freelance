Title: Ändert eure Passwörter!

----

Link: https://github.com/pirate/sites-using-cloudflare

----

Short: 

Heute wurde bekannt das (link: https://blog.cloudflare.com/incident-report-on-memory-leak-caused-by-cloudflare-parser-bug/ text: Cloudflare) über einen längeren Zeitraum einen Sicherheitslücke hatte. Cloudflare kennst du nicht / nutzt du nicht? Egal - Cloudflare wird von vielen Webseiten als CDN genutzt die du wahrscheinlich täglich besuchst, wie z.B. Producthunt, Medium, Codepen, Stackoverflow, Patreon oder Hackernews. 

> **Between 2016-09-22 - 2017-02-18 passwords, private messages, API keys, and other sensitive data were leaked by Cloudflare to random requesters. Data was cached by search engines, and may have been collected by random adversaries over the past few months.**

> "The greatest period of impact was from February 13 and February 18 with around 1 in every 3,300,000 HTTP requests through Cloudflare potentially resulting in memory leakage (that’s about 0.00003% of requests), potential of 100k-200k paged with private data leaked every day" -- (link: https://news.ycombinator.com/item?id=13719518 text: source)

> You can see some of the leaked data yourself in search engine caches: (link: https://duckduckgo.com/?q=+%7B%22scheme%22%3A%22http%22%7D+CF-Host-Origin-IP&t=h_&ia=web text: https://duckduckgo.com/?q=+%7B%22scheme%22%3A%22http%22%7D+CF-Host-Origin-IP&t=h_&ia=web)

Auf (link: https://github.com/pirate/sites-using-cloudflare text: Github wurde das Problem beschrieben) und eine (link: https://github.com/pirate/sites-using-cloudflare/archive/master.zip text: komplette Liste mit allen gefährdeten Seiten) ist dort in einer Liste zum Download (22mb!) vorhanden. Ausserdem ist ein Abgleich mit den 1000 meistbesuchten Seiten von Alexa.com vorhanden. **Geht die Liste durch und ändert Passwörter dementsprechend! **

----

Date: 2017-02-24

----

Author: florenz

----

Tags: security

----

Category: linkpost