Title: Workshops und Schulungen

----

Text: Themen die ich u.a. abdecke: *Skalierbare CSS/SCSS Architekturen und Best-Practices, Arbeiten mit Git, Atomic Design mit Patternlab.*

----

Blockorder: 1

----

Dateline: Da ich mich sehr viel mit neuen Webtechnologien, Best-Practices und Workflows beschäftige, habe ich mir inzwischen eine Menge Wissen angeeignet. Bei hmmh habe ich bereits erfolgreich viele **Schulungen** und **Vorträge** vorbereitet und umgesetzt und konnte Wissen mit anschaulichen Beispielen weitergeben.

----

Svgimage: 

----

Sectionid: workshops

----

Sectionname: Schulungen

----

Minheight: full