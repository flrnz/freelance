Title: Florenz Heldermann

----

Author: Florenz Heldermann

----

Description: 

Moin, ich bin Florenz Heldermann und mache Webseiten. Ich bin Frontend und Interface Developer und habe einen starken Fokus auf UX und Gestaltung. Ich bin immer auf der Suche nach spannenden und interessanten Projekten. 
Schreibt mir eine (email: hi@flore.nz text: Mail), auf (link: https://www.xing.com/profile/Florenz_Heldermann text: schreibt mir auf Xing) oder  folgt mir auf (link: http://twitter.com/track02 text: Twitter).

----

Keywords: Freelance,Webdev,HTML,CSS3,Patternlab,Atomic Design,Styleguide,Performance,Freelancer,Freiberufler

----

Copyright: © 2014-(date: Year) (link: http://flore.nz text: Florenz Heldermann) | made with (link:http://getkirby.com text:Kirby) | hosted on (link: http://uberspace.de text: Uberspace) | (link:/impressum text:Impressum)  | (link:/datenschutz text:Datenschutz)

----

Email: hi@flore.nz

----

Phone: 0176 610 999 09

----

Twitter: track02

----

Authorimage: 167692_10150173087568018_6112188_n.jpg

----

Ancientbrowser: 

Du verwendest scheinbar einen sehr alten Browser. Diese Seitenansicht **wird** in deinem Browser kaputt aussehen. Bitte aktualisiere bei Gelegenheit deinen Browser. Solltest du auf einem veralteten Betriebssystem sein probier doch mal (link: https://www.mozilla.org/de/firefox/new/ text: Firefox), (link: http://chrome.google.com text: Chrome), (link: https://vivaldi.com/ text: Vivaldi) oder (link: http://www.opera.com/ text: Opera). Solltest du deinen Browser nicht updaten können (*Arbeitsplatzrechner*, *keine Rechte*), kontaktiere einen Administrator und weise ihn auf die (link: http://www.tecchannel.de/a/unterschaetzte-gefahr-schwachstelle-browser,2036971 text: potentiellen Sicherheitsrisiken) hin.

Ich könnte  diese Seite mit etwas Zeitaufwand auch für alte Browser optimieren. Allerdings möchte ich nicht meine Zeit dafür opfern, wenn selbst Browserhersteller ein Produkt nicht mehr supporten. Im schlimmsten Fall sind solche Anpassungen schädlich für neuere Browser und **dann ist keiner zufrieden**.

Nein. Dieser Hinweis lässt sich nicht wegklicken. *Sorry*.