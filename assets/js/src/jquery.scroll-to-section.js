var $ = require('jquery');

module.exports = {
    init: function() {
        // Scroll to Section

        $('.js-scroll').on('click', function(){
            var target = $(this).attr('href');
            $('html, body').animate({
                'scrollTop':   $(target).offset().top
            }, 800);
            return false;
        });
    }
}
