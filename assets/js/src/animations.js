var $ = require('jquery');
var TweenMax = require('TweenMax');
var TimelineLite = require('TimelineLite');
var TimelineMax = require('TimelineMax');
var ScrollMagic = require('ScrollMagic');
require('animation.gsap');

module.exports = {
    init: function() {
        // init scrollmagic 
        // var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
        var controller = new ScrollMagic.Controller();
        
        // simple textanimation effect
        $('.text--gsap').each(function(){
            var currentElement = this;
            new ScrollMagic.Scene({triggerElement: this, triggerHook: 0.7, duration: 150})
                .setTween($(this).find('hr'),
                        { width: "50%" }) // the tween durtion can be omitted and defaults to 1
                .addTo(controller);
            new ScrollMagic.Scene({triggerElement: this, triggerHook: 0.7, duration: 150})
                .setTween($(this).find('h1'),
                        { opacity: "1", y:-5, scale: 1 }) // the tween durtion can be omitted and defaults to 1
                .addTo(controller);
            new ScrollMagic.Scene({triggerElement: this, triggerHook: 0.7, duration: 150})
                .setTween($(this).find('.pane__text'),
                        { opacity: "1", y:5, scale: 1 }) // the tween durtion can be omitted and defaults to 1
                .addTo(controller);
        });

        // sm icon show up
        if ( $('#contact').length ) {
            var icontween = TweenMax.staggerFromTo('.iconlist > li', 0.5,
                { y: -90, opacity: 0},
                {
                    scale: 1,
                    opacity: 1,
                    y: 0,
                    ease: Back.easeOut
                },
                0.1);

            var icons = new ScrollMagic.Scene(
                {triggerElement: "#contact", triggerHook: 0.35, duration: 300})
                .setTween(icontween)
                .addTo(controller);
        }
        

        if ( $('#display').length ) {
            // display animation
            var display = $('#display'),
                displayCode = $('#display #code'),
                dLine1 = $('#display #line1')
                dLine2 = $('#display #line2')
                dLine3 = $('#display #line3')
                dLine4 = $('#display #line4')
                dLine5 = $('#display #line5')
                dLine6 = $('#display #line6')
                dLine7 = $('#display #line7')
                dLine8 = $('#display #line8')
                dLine9 = $('#display #line9')
                dLine10 = $('#display #line10');

            var displayAnimation = new TimelineLite();
                displayAnimation.fromTo(dLine1, 1.5, 
                    {strokeDasharray: 400, strokeDashoffset: 400},
                    {strokeDashoffset: 0}
                );
                displayAnimation.fromTo(dLine8, 0.9, 
                    {strokeDasharray: 450, strokeDashoffset: 450},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.fromTo(dLine2, 0.5, 
                    {strokeDasharray: 400, strokeDashoffset: 400},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.fromTo(dLine3, 0.9, 
                    {strokeDasharray: 400, strokeDashoffset: 400},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.fromTo(dLine4, 0.9, 
                    {strokeDasharray: 450, strokeDashoffset: 450},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.fromTo(dLine5, 0.9, 
                    {strokeDasharray: 450, strokeDashoffset: 450},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.fromTo(dLine6, 0.9, 
                    {strokeDasharray: 450, strokeDashoffset: 450},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.fromTo(dLine7, 0.9, 
                    {strokeDasharray: 450, strokeDashoffset: 450},
                    {strokeDashoffset: 0}, '-=0.5'
                );
                displayAnimation.to(display, 5, {rotationY: -20}, '-=2');
        }


        // character animation
        if( $('#me').length ) {
            var meBody = $('#meflrnz'),
                meHead = $('#meflrnz #head'),
                meArmL = $('#meflrnz #arm_l'),
                meArmR = $('#meflrnz #arm_r'),
                meLegL = $('#meflrnz #leg_l'),
                meLegR = $('#meflrnz #leg_r');

            var meTween = new TimelineLite({onComplete: randomInteraction});
                meTween.fromTo(meBody, 0.6, {ease: Power4.easeOut, y: '105%'}, {rotation: -7, y: '-15%'});
                meTween.to(meBody, .3, {ease: Power4.easeOut, y: '5%', rotation: 0});
                meTween.to(meArmL, .3, {rotation: 40, transformOrigin: "50% 0%"}, '0.25');
                meTween.to(meArmR, .3, {rotation: -50, transformOrigin: "50% 0%"}, '0.25');
                
                meTween.to(meLegL, .3, {rotation: 40, transformOrigin: "50% 0%"}, '0.25');
                meTween.to(meLegL, .3, {rotation: 0, transformOrigin: "50% 0%"}, '0.55');
                
                meTween.to(meLegR, .3, {rotation: -30, transformOrigin: "50% 0%"}, '0.25');
                meTween.to(meLegR, .3, {rotation: 0, transformOrigin: "50% 0%"}, '0.65');

                meTween.to(meArmL, .5, {rotation: 0, transformOrigin: "50% 0%"});
                meTween.to(meArmR, .5, {rotation: 0, transformOrigin: "50% 0%"}, '-=.5');
            
            var flrnz = new ScrollMagic.Scene(
                {triggerElement: '#me', triggerHook: 0.4})
                .setTween(meTween)
                .addTo(controller);
        }

        function randomInteraction() {
            var rand = Math.round(Math.random() * (3000 - 500)) + 5000;
            setTimeout(function() {
                var randAnimation = Math.random();
                if (randAnimation < 0.4) { headRight(); }
                else if (randAnimation < 0.85) { headLeft(); }
                else { kranichStil(); }
            }, rand);
        };

        function headRight(){
            TweenMax.to(meHead, 2, { rotation:5, transformOrigin: "50% 100%", yoyo:true, repeat: 1, onComplete: randomInteraction});
        }
        
        function headLeft(){
            TweenMax.to(meHead, 2, { rotation:-7, transformOrigin: "50% 100%", yoyo:true, repeat: 1, onComplete: randomInteraction});
        }
        
        function kranichStil(){
            var kranich = new TimelineLite({onComplete: randomInteraction});
                kranich.to(meLegR, 2, {rotation: -70});
                kranich.to(meArmR, 2, {rotation: -110}, '-=2');
                kranich.to(meArmL, 2, {rotation: 100}, '-=2');

                kranich.to(meLegR, 1, {rotation: 0}, '4');
                kranich.to(meArmR, 1, {rotation: 0}, '-=2');
                kranich.to(meArmL, 1, {rotation: 0}, '-=2');
        }

        // SMARTPHONE ANIMATION

        if ( $('#phoneTop').length ) {
            var smartphoneTween = new TimelineLite(); 
                smartphoneTween.from( '#phoneTop', .1, {y: '200px', opacity: 0});
                smartphoneTween.from( '#phoneTop #wireframeInner >g', .2,
                                        {ease: Power4.easeOut, y: '-100px'}
            );
        }

        if ( $('#frontend').length ) {
            var smartphone = new ScrollMagic.Scene(
                {triggerElement: '#frontend', triggerHook: 0.55, duration: 400})
                .setTween(smartphoneTween)
                .addTo(controller);
        }
    

        // ATOMIC ANIMATION
        if ( $('#ATOMIC').length ) {
            var atomicAnimation = new TimelineMax({
                paused:false, 
                yoyo: true, 
                delay:0 });

            var aBlocks = $('#ATOMIC'),
                aBlock1 = $('#ATOMIC #BLOCK1'),
                aBlock1Bg = $('#ATOMIC #BLOCK1 #BG1'),
                aBlock1Hd = $('#ATOMIC #BLOCK1 #Headline'),
                aBlock1Tx = $('#ATOMIC #BLOCK1 #Paragraph_1_'),
                aBlock1Img = $('#ATOMIC #BLOCK1 circle'),
                
                aBlock2 = $('#ATOMIC #BLOCK2'),
                aBlock2Bg = $('#ATOMIC #BLOCK2 #BG2'),
                aBlock2P1 = $('#ATOMIC #BLOCK2 #ParagraphSM1'),
                aBlock2P2 = $('#ATOMIC #BLOCK2 #ParagraphSM2'),
                aBlock2P3 = $('#ATOMIC #BLOCK2 #ParagraphSM3'),
                aBlock2Hd1 = $('#ATOMIC #BLOCK2 #B2HeadlineSmall1'),
                aBlock2Hd2 = $('#ATOMIC #BLOCK2 #B2HeadlineSmall2'),
                aBlock2Hd3 = $('#ATOMIC #BLOCK2 #B2HeadlineSmall3'),
                
                aBlock3 = $('#ATOMIC #BLOCK3'),
                aBlock3Bg = $('#ATOMIC #BLOCK3 #BG1_1_'),
                aBlock3Hd = $('#ATOMIC #BLOCK3 #HEADLINE3'),
                aBlock3Tx = $('#ATOMIC #BLOCK3 #Paragraph3'),
                clone1 = $('#ATOMIC #clone1'),
                clone2 = $('#ATOMIC #clone2'),
                backCircle = $('#backcircle');
            
            atomicAnimation.set(aBlocks, {
                transformOrigin: "50% 50%"
            })
            atomicAnimation.set(backCircle, {
                transformOrigin: "50% 50%"
            })
            
            atomicAnimation
                .add("start", 0)
                .add("block1complete", .5)
                .add("block2complete", 1.25)
                .add("block3complete", 2)
                .add("pageComplete", 2.75)

                .from( aBlock1Hd, .25, { y: '350', x: '490', rotation: 30}, 'start')
                .from( aBlock1Tx , .5, { y: '400', x: '270', scale: .7 }, 'start')
                .from( aBlock1Img , .5, { y: '430', x: '270', scale: 1.2 }, 'start')
                .from( aBlock1Bg, .5, { scaleX: 0.2, scaleY: .3, x: '600', y: 700 }, 'start')

                .from(aBlock2P1, .3, { y: '400', x: '150' }, 'block1complete')
                .from(aBlock2P2, .3, { x: '-150', opacity: 0 }, 'block1complete')
                .from(aBlock2P3, .3, { x: '-150', opacity: 0 }, 'block21omplete')
                .from(aBlock2Bg, .5, { scaleX: 0.2, scaleY: .5, x: '600', y: 445, rotation: 50 }, '-=0.6')
            
                .from(aBlock3Tx, .5, { y: '-450', opacity: 0 }, 'block2complete')
                .from(aBlock3Hd, .5, { y: '-450', opacity: 0 }, 'block2complete')
                .from(aBlock3Bg, .5, { scaleX: 0.2, scaleY: .4, x: '600', y: 0 }, 'block2complete')

                .from(aBlock1, 1, { y: '-100', x: '-100' ,ease: Power3.easeOut }, 'block3complete')
                .from(aBlock2, 1, { y: '-30', x: '70', ease: Power3.easeOut }, 'block3complete')
                .from(aBlock3, 1, { y: '100', x: '70', ease: Power3.easeOut }, 'block3complete')
            
                .to( aBlocks, .25, { scale: 1.1, ease: Back.easeOut}, 'pageComplete')
                .to( aBlocks, .25, { scale: 1.05}, 'pageComplete' + .25)
            
                .from( backCircle, .25, { scale: 0, ease: Back.easeOut}, '-=.65')
                //.to( backCircle, .15, { scale: 1.05}, 'pageComplete' + .35)

                .from( aBlock1Img , .25, { fill: "#aaa" }, 'pageComplete')
                .from( aBlock1Hd, .25 , {fill : "black"}, 'pageComplete')
                .from( aBlock2Hd1, .25, {fill : "black"}, 'pageComplete')
                .from( aBlock2Hd2, .25, {fill : "black"}, 'pageComplete')
                .from( aBlock2Hd3, .25, {fill : "black"}, 'pageComplete')
                .from( aBlock3Bg, .25,  {fill : "#bbb"}, 'pageComplete')
                .from( aBlock3Hd, .25, {fill : "black"}, 'pageComplete')
            ;

            // atomicAnimation.to(aBlocks, 2, {scale: 1.3, ease: Elastic.easeOut.config(1, 0.3)}, '-=1');
            // atomicAnimation.to(aBlocks, .7, {rotation: 5});
            atomicAnimation.to(clone1, .5, { opacity: 1 }, '-=.5');
            atomicAnimation.to('#innerShadow', 0.5, {
                attr: {
                stdDeviation: 45
                }
            });

            var atomicScene  = new ScrollMagic.Scene(
                {triggerElement: "#ATOMIC", triggerHook: 0.55 })
                .setTween(atomicAnimation)
                .addTo(controller);
        }


        // CONTACT FLRNZ CIRCLE ANIMATION
        if ( $('#CIRCLE_FLRNZ').length ) {
            var contactSet = new TimelineMax();
            var contactAnimation = new TimelineMax({
                    pause: false,
                    yoyo:true, 
                    delay: 0 });
            
            var head = $('#CIRCLE_FLRNZ #florenz');
            var cOuter = $('#CIRCLE_FLRNZ #outer_circle');
            var cInner = $('#CIRCLE_FLRNZ #inner_circle');
            var browLeft = $('#CIRCLE_FLRNZ #eyebrow_left');
            var browRight = $('#CIRCLE_FLRNZ #eyebrow_right');
            var hat = $('#CIRCLE_FLRNZ #hat');

            contactSet.set(head, {scaleX: 0, scaleY: 0, transformOrigin: '50% 50%', visibility: 'visible'})
            .set(cOuter, {scaleX: 0, scaleY:0, transformOrigin: '50% 50%'})
            .set(cInner, {scaleX: 0, scaleY:0, transformOrigin: '50% 50%'})
            ;
            
            contactAnimation.to(cInner, .6, {
                scaleX: 1, 
                scaleY: 1,
                ease: Circ.easeOut
            })
            .to(cOuter, .5, {
                scaleX: 1, 
                scaleY: 1, ease: 
                Circ.easeOut 
            }, "-=0.5")
            .to(head, .5, {
                scaleX: 1, 
                scaleY: 1, 
                ease: Power4.easeInOut 
            }, "-=0.6")
            .to(browRight, .25, {
                y: "-=10",
                ease: Power3.easeInOut 
            })
            .to(hat, .5, {
                transformOrigin: '6 10',
                rotation: -3,
                x: "-=10",
                ease: Power4.easeInOut 
            }, "-=0.4")
            ;

            var contactScene  = new ScrollMagic.Scene({
                    triggerElement: "#CIRCLE_FLRNZ", 
                    triggerHook: 0.50 
                })
                .setTween(contactAnimation)
                .addTo(controller);
        }


        // ICONLIST ANIMATION
        $('.iconlist > li').hover(scaleUp, scaleDown);
        function scaleUp(){
            TweenLite.fromTo($(this), .2, 
                {scale: "1"},
                {scale: "1.2", rotation: "-20", ease: Back.easeInOut}
            );
        }
        function scaleDown(){
            TweenLite.fromTo($(this), .2, 
                {scale: "1.2"},
                {scale: "1", rotation: "0"}
            );
        }
        
        if ( $('#CIRCLE_FLRNZ').length ) {
            var coffeeSVG = $('#Kaffee'),
                coffeeInner = $('#Kaffee #cInner');

            TweenLite.set( coffeeInner, {transformOrigin: '50% 50%'});
            var coffeeAnimation = new TimelineLite();
                coffeeAnimation.from(coffeeSVG, .5, {x:"-100%"});
                coffeeAnimation.to(coffeeInner, 10, {rotation: '360', repeat: 33});
        }
        
        if( $('.workshop').length ) {
            var coffee  = new ScrollMagic.Scene(
                {triggerElement: ".workshop", triggerHook: 0.55 })
                .setTween(coffeeAnimation)
                .addTo(controller);
        }
    }
}

