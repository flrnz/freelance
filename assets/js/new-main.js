var hello = require('./src/console-hello.js');
var scrollToSections = require('./src/jquery.scroll-to-section.js');
var msieVersion = require('./src/jquery.msie-version.js');
var animations = require('./src/animations.js');

hello.init();
scrollToSections.init();
msieVersion.init();
animations.init();
